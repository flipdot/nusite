+++
title = "Infrastruktur"
+++

| System                  | Admin         | Beteiligte                              |
| ----------------------- | ------------- | --------------------------------------- |
| Router                  | Malled        | -                                       |
| GUI-Raspi               | Gamescripting | -                                       |
| Display-Raspi           | Gamescripting | Sören                                   |
| GSM-Türschloss          | typ.o         | -                                       |
| [Tür-Raspi]             | typ.o         | oboro                                   |
| [CAN-Raspi]             | typ.o         | Gamescripting                           |
| [Getränkezähler]-Raspi  | DoB           | DoB, Niklas, Sören, Anselm, Eike, Jonas |
| Watts-up-Pi Stromzähler | typ.o         | typ.o, Burkard, Sören                   |
| [3D-Drucker]            |               | DmB, Sägewerk                           |

[Tür-Raspi]: /projekte/tür/
[CAN-Raspi]: /projekte/can-raspi/
[Getränkezähler]: /projekte/getränkezähler/
[3D-Drucker]: /projekte/3d-drucker/
