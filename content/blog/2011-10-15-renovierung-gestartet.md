+++
title = "Renovierung gestartet"
date = 2011-10-15T18:57:34Z
author = "typ_o"
path = "/blog/2011/10/15/renovierung-gestartet"
aliases = ["/blog/archives/148-Renovierung-gestartet.html"]
+++
Heute Schritt eins, sollte eigentlich Wände anmalen sein, aber:
Grauenhafte Entdeckung in der Wand: Strom war auf dem Standard von 1930
- Alu-Doppeleiter in Zinkrohr, Nullung, teilweise mit PE, dafür dann
aber einzelne Steckdosen mit Rückleiter im PE, omfg!  
Also die Hälfte rausgerissen...  
Dafür gehts mit dem RFID-Türschloss zügig voran, Software steht,
Controllerhardware 80%, Motorantrieb für den Riegel 80%.
