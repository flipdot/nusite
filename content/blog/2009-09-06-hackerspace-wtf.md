+++
title = "Hackerspace - WTF?"
date = 2009-09-06T12:52:00Z
author = "typ_o"
path = "/blog/2009/09/06/hackerspace-wtf"
aliases = ["/blog/archives/1-Hackerspace-WTF.html"]
+++
Ein Hackerspace (von Hacker und Space, engl. für Raum) ist ein realer,
oft offener Raum, in dem sich technisch-kreative Menschen treffen und
austauschen. Oft werden Themen rund um (aber nicht beschränkt auf)
Wissenschaft, Computer, Internet, Netzwerke, Elektronik und
Programmierung behandelt.  

Typische Aktivitäten in einem Hackerspace sind

- Lernen und Wissen teilen
- Präsentationen und Vorführungen
- Soziale Aktivitäten, Partys, Spiele

Der Hackerspace stellt hier die Infrastruktur bereit. Das ist meist
Strom, ein Internetzugang, Netzwerkverbindungen, Räume, Werkzeug und
Getränke.

Meist ist beim Eintritt keine Gebühr zu entrichten. Da die
Bereitstellung der Infrastruktur aber Geld kostet, wird meist um eine
Mitgliedschaft gebeten. Oft werden die Räumlichkeiten auch anderen
Gruppen aus demselben Themenbereich zur Verfügung gestellt.

Bekannte Hackerspaces sind beispielsweise C4 in Köln, c-base in Berlin,
Das Labor in Bochum, Metalab und Happylab in Wien, HackerbotLabs in
Seattle, HacDC in Washington DC, NYC Resistor \[1\] in New York City und
Noisebridge \[2\] in San Francisco. (Q:
[Wikipedia](https://de.wikipedia.org/wiki/Hackerspace))
