+++
title = "Knock-knock-knock-Penny (Opening hackerspaces with internet memes)"
date = 2013-01-16T03:30:08Z
author = "typ_o"
path = "/blog/2013/01/16/knock-knock-knock-penny-opening-hackerspaces-with-internet-memes"
aliases = ["/blog/archives/178-Knock-knock-knock-Penny-Opening-hackerspaces-with-internet-memes.html"]
+++
Member calls space lock with his cellphone. As the incoming call ist
detected the controller rejects the call (Good old "ath"). Caller ID is
checked against members list and lock is then waiting for the magic
knocks. [See Wiki page for
Details](https://web.archive.org/web/20111031013858/https://flipdot.org/wiki/index.php?title=Zugangssystem)
(german).
