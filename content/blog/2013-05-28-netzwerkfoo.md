+++
title = "Netzwerkfoo"
date = 2013-05-28T19:33:00Z
author = "typ_o"
path = "/blog/2013/05/28/netzwerkfoo"
aliases = ["/blog/archives/206-Netzwerkfoo.html"]
+++
Unseren Powerline-Adapter haben wir letzte Woche wohl kaputtgespielt,
jetzt werkelt ein einfacher WiFi Range-Extender. So können wir
wenigstens den Raspberry an der Türsteuerung am Netz haben.
