+++
title = "Heartbeat"
date = 2020-09-06T18:35:49Z
author = "Baustel"
path = "/blog/2020/09/06/heartbeat"
aliases = ["/blog/archives/444-Heartbeat.html"]
+++
Ja, den flipdot gibt es noch und wir sind auch alle munter und wohl
auf.  
An dieser Stelle wollen wir aufgrund häufiger Nachfragen noch einmal auf
unsere aktuelle Regelung in der Corona-Zeit hinweisen.

Es finden leider weiterhin keine offenen Veranstaltungen statt. Darunter
fällt auch unser regulärer offener Dienstag.  
Interessierte können sich gerne bei uns im Chat melden, wo wir uns
aktuell, virtuell des öfteren treffen.  
Kontaktmöglichkeiten findet ihr wie gehabt hier: [Kontakt](/kontakt/)
