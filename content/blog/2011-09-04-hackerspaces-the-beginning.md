+++
title = "HackerSpaces: The Beginning!"
date = 2011-09-04T10:00:26Z
author = "typ_o"
path = "/blog/2011/09/04/hackerspaces-the-beginning"
aliases = ["/blog/archives/146-HackerSpaces-The-Beginning!.html"]
+++
Bre Pettis, Astera Schneeweisz, and Jens Ohlig wrote a book which
documents where the hackerspace movement was in December of 2008. In
that way it’s a bit of a time capsule. [Download The
Beginning](https://blog.hackerspaces.org/2011/08/31/hackerspaces-the-beginning-the-book/).

[![](/media/beginning.jpg)](https://blog.hackerspaces.org/2011/08/31/hackerspaces-the-beginning-the-book)
