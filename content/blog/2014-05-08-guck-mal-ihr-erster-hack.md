+++
title = "Guck mal, ihr erster Hack!"
date = 2014-05-08T16:28:32Z
author = "typ_o"
path = "/blog/2014/05/08/guck-mal-ihr-erster-hack"
aliases = ["/blog/archives/241-Guck-mal,-ihr-erster-Hack!.html"]
+++
[Jugend hackt 2014](https://jugendhackt.de/): 12.-14. September 2014,
gemeinsam hacken und mit Code die Welt zu verbessern. Glingt ganz
vernünftig.
