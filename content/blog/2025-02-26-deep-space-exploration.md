+++
title = "Deep Space Exploration"
date = 2025-02-26T11:29:35.722Z
author = "and…"
path = "/blog/2025/02/15/Deep-Space-Exploration"
draft = false
+++

am 
## Tag der offenen Hackspaces

Die [Erfas- und Chaostreffs](https://www.ccc.de/regional) des CCC und weitere [Maker- und Hackspaces](https://hackerspaces.org) öffnen ihre Türen explizit für neugierige Besucher und stellen sich vor. Natürlich auch in Deiner Nähe!
</br>
___
Am **Samstag, 2025-03-29 ab 13:37** bis Open End </br>
flipdot, Schillerstraße 25, 34117 Kassel
___
</br>
Im freundlichen Hackspace in Deiner Nachbarschaft, kannst Du Dir an dem Tag ganz hemmungslos alles anschauen und auch bei vielem mitmachen. 

[Falls doch noch unklar ist wer wir sind und was wir so tun.](https://flipdot.org/faq/)

Du kannst deine ersten Lötstellen herstellen und die Ergebnisse mit nach Hause nehmen, es gibt spannende Vorträge und der 3D Drucker wird fröhliche vor sich hin surren.
Vorkenntnisse und Qualifikationen sind exakt gar keine nötig, nur ein bisschen Offenheit und Neugierde.
Also komm vorbei, schnupper in unsere Welt und lass Dich von unserer Hacker- und Maker-Leidenschaft anstecken!

Wir wuseln jetzt erst mal weiter um das ganze vorzubereiten.



Wir freuen uns auf Dich! 🤖🛠️💡

![](/media/deep_space3.jpg)
