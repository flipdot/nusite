+++
title = "Illustration im Triebwerk 9"
date = 2020-02-28T15:09:34Z
author = "Baustel"
path = "/blog/2020/02/28/illustration-im-triebwerk-9"
aliases = ["/blog/archives/437-Illustration-im-Triebwerk-9.html"]
+++

Wir haben eine Anzeige im [Triebwerk
9](https://www.startnext.com/triebwerk-9) aufgegeben. Das Ergebnis fasst
den üblichen Dienstag im Space graphisch super zusammen:

![](/media/AnzeigeTriebwerkinFarbpaletteMIMend..serendipityThumbSmall.serendipityThumb.jpg)

Die Koloration basiert auf der vorherigen
[Skizze](/media/AnzeigeTriebwerksketchMIMend..PNG).
Vielen Dank an MIMend für die tolle Umsetzung. Mehr gibts auf [ihrem
Instagram](https://www.instagram.com/mim.end/) und [ihrer
Webseite](https://www.mimend.com/) zu sehen.
