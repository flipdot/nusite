+++
title = "Kontakt / Impressum"
date = 2009-06-13T07:12:00Z
author = "typ_o"
path = "/blog/2009/06/13/kontakt-impressum"
aliases = ["/blog/archives/12-Kontakt-Impressum.html"]
+++
Diese Seite ist das Blog des Vereins flipdot e.V.

zu erreichen über:  
c/o Marko Ledesma  
Lange Strasse 24  
34131 Kassel.

und über [weitere
Kontaktmöglichkeiten](https://flipdot.org/blog/archives/13-Communication.html).

Die Vereinsräume sind hier:

flipdot. e.V.  
Sickingenstraße 10  
34117 Kassel
