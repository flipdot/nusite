+++
title = "Demo gegen Zensur-Infrastruktur Kassel am  9.3.ab 13:00"
date = 2019-03-06T20:48:18Z
author = "typ_o"
path = "/blog/2019/03/06/demo-gegen-zensur-infrastruktur-kassel-am-9-3-ab-13-00"
aliases = ["/blog/archives/427-Demo-gegen-Zensur-Infrastruktur-Kassel-am-9.3.ab-1300.html"]
+++
[![](/media/D0_FesZWkAEi1NT.serendipityThumb.png)](/media/D0_FesZWkAEi1NT.png)

Die Demo richtet sich unter Anderem gegen Uploadfilter. Warum diese ein
Problem werden können, erklärt dieses Video:

Info vom Chaos Computer Club
[hier](https://www.ccc.de/de/updates/2019/article13).
