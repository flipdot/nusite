+++
title = "Schneidklemmverbinder, ick liebe dir! (GSM Türschloss)"
date = 2013-01-11T16:21:07Z
author = "typ_o"
path = "/blog/2013/01/11/schneidklemmverbinder-ick-liebe-dir-gsm-turschloss"
aliases = ["/blog/archives/177-Schneidklemmverbinder,-ick-liebe-dir!-GSM-Tuerschloss.html"]
+++
![](/media/schneidklemm.jpg)

Auf dem Weg zum GSM gesteuerten Türschloss. Derzeit öffnen wir mit einer
Bluetooth-SPP Terminalverbindung, die allerdings iOS-User ausschließt,
da Apple kein SPP Profil erlaubt. Der nächste Iterationsschritt zum
perfekten Schloss nach RFID und jetzt Bluetooth wird GSM sein. SMS mit
Passcode an das Schloss, das prüft Caller ID und Passcode, und öffnet.
