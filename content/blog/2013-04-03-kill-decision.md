+++
title = "Kill Decision"
date = 2013-04-03T06:35:38Z
author = "typ_o"
path = "/blog/2013/04/03/kill-decision"
aliases = ["/blog/archives/188-Kill-Decision.html"]
+++
![](/media/bigdog.serendipityThumb.jpg)Daniel
Suarez' neuer Roman ist jetzt übersetzt. Bewaffnete Drohnen werden gegen
Forscher eingesetzt, und die Helden versuchen herauszubekommen, wer
dahinter steckt. Liest sich flüssig weg, nicht ganz so viele brilliante
Ideen wie in Daemon und Darknet, dafür bleiben einem auch die Motorräder
mit Schwertern erspart. Gut herausgearbeitet: Es gibt keine einzelnen
Bösen mehr, nur Interessen, die von Social Media Manipulateuren mit
einer Armee von Sockenpuppen gestützt werden. Fragestellungen wie
"Welche Eskalationsstufen werden durch Drohnen ausgelöst". Read more:
[Längere
Kritik](https://www.faz.net/aktuell/feuilleton/buecher/thriller-kill-decision-von-daniel-suarez-wie-technik-die-welt-zum-schlechteren-wendet-11826693.html)
in der FAZ, [Frank und Fefe zu Drohnen](https://alternativlos.org/27/) in
Alternativlos, Frank Rieger auf dem netzpolitischen Abend der Digitalen
Gesellschaft [über Drohnen und
Politik](https://www.youtube.com/watch?v=M7XphxoA3MU),
[niedlich](https://www.youtube.com/watch?v=YQIMGV5vtd4) wirkende
Quadrocopter, der [Big Dog](https://www.youtube.com/watch?v=W1czBcnX1Ww)
von Boston Dynamics beim [Steine
werfen](https://www.youtube.com/watch?v=9dZ3KwczrI8), und ein regelrecht
begeistertes Feature über ein [autonomes
Kampffahrzeug](https://www.youtube.com/watch?v=WOD5NF48byo).
