+++
title = "Tag der offenen Tür am Samstag, 26.6.2010"
date = 2010-06-20T21:57:39Z
author = "typ_o"
path = "/blog/2010/06/20/tag-der-offenen-tur-am-samstag-26-6-2010"
aliases = ["/blog/archives/94-Tag-der-offenen-Tuer-am-Samstag,-26.6.2010.html"]
+++
[![](/media/opendoor1.serendipityThumb.jpg)](/media/opendoor1.pdf)Am
kommenden Samstag ist in dem [Hausprojekt](https://haus-chasalla.de/) in
der
[Sickingenstraße](https://maps.google.com/maps?f=q&source=s_q&hl=de&geocode=&q=sickingenstrasse+10,+kassel&sll=51.320193,9.495353&sspn=0.001224,0.005493&g=51.320238,9.495471&ie=UTF8&hq=&hnear=Sickingenstra%C3%9Fe+10,+Kassel+34117+Kassel,+Hessen,+Deutschland&ll=51.320465,9.495202&spn=0.001224,0.005493&z=18),
in dem wir unsere Räume haben, Tag der offenen Tür. Alle Gruppen und
Initiativen stellen ihre Arbeit vor - bei uns sieht das natürlich etwas
anders und chaotischer aus. Kommt und guckt euch work in progress an,
lötet euch was, schraubt was auf, habt gute Gespräche über Technik und
plant neue Dinge. Wer eine gute Gelegenheit sucht, sich den Hackerspace
mal eben so anzugucken, findet diese am Samstag. Im Hof gibts auch
Verpflegung. Die [Einladung als
pdf](/media/opendoor1.pdf). Bitte in euren
Kreisen verteilen!
