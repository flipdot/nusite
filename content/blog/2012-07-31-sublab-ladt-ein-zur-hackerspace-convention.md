+++
title = "Sublab lädt ein zur Hackerspace Convention"
date = 2012-07-31T18:21:05Z
author = "typ_o"
path = "/blog/2012/07/31/sublab-ladt-ein-zur-hackerspace-convention"
aliases = ["/blog/archives/169-Sublab-laedt-ein-zur-Hackerspace-Convention.html"]
+++
Lustig, diese Postkarten immer zu kriegen, ein schöner Brauch! Die 3.
Large Hackerspace Convention ist vom 31.8. bis zum 2.9. in Leipzig.

[![](/media/IMAG1859.serendipityThumb.jpg)](/media/IMAG1859.jpg)
