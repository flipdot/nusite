+++
title = "5 Min - Hack: Ortlieb Tasche am Brompton - Lenker"
date = 2013-06-09T17:37:48Z
author = "typ_o"
path = "/blog/2013/06/09/5-min-hack-ortlieb-tasche-am-brompton-lenker"
aliases = ["/blog/archives/208-5-Min-Hack-Ortlieb-Tasche-am-Brompton-Lenker.html"]
+++
Weil ich zu der [Raspberry Pi - Veranstaltung in
Trier](https://www.piandmore.de/) natürlich per Bahn und
[Faltrad](https://www.brompton.de/) anreisen will, und ein wenig
Equipment brauche, hier der 5-Minuten Hack für eine Fronthalterung für
eine Ortlieb - Packtasche am Lenker.

![](/media/ortl01.jpg)

Material: 8 mm - Rundmaterial (Hier Messing weil vorhanden) passend für
die Ortlieb - Aufnahmen, Kabelbinder, zwei Schnappschellen für
Installationsrohr (was der Elektrolurch halt so im Keller hat)

Und - es läßt sich noch 1a zusammen falten! Belastungstest mit neun
Litern Wasser bestanden.

![](/media/ortl02.jpg)\

![](/media/ortl03.jpg)

![](/media/ortl04.jpg)
