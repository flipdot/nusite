+++
title = "Tip für Fräswerkstücke, die nicht in den Arbeitsbereich passen (Passbolzen zum Wenden)"
date = 2015-08-03T15:32:00Z
author = "typ_o"
path = "/blog/2015/08/03/tip-fur-fraswerkstucke-die-nicht-in-den-arbeitsbereich-passen-passbolzen-zum-wenden"
aliases = ["/blog/archives/318-Tip-fuer-Fraeswerkstuecke,-die-nicht-in-den-Arbeitsbereich-passen-Passbolzen-zum-Wenden.html"]
+++
[![](/media/20150727_145638.serendipityThumb.jpg)](/media/20150727_145638.jpg)

Mindestens zwei Bohrungen so anlegen, dass sie auch bei 180 Drehung die
Position behalten (spiegelsymmetrisch). Dann bei der ersten
Werkstückhälfte ganz durch das Werkstück und fast ganz durch die
Opferplatte bohren.

Nach dem Wenden lässt sich das Werkstück dann z.B. mit zwei Bohrern als
Bolzen vor dem Spannen ausrichten. Wenn man 6 mm Bohrungen brauchen
kann, dann eignen sich die Bolzen von Ikea-Ivar-Regalen ganz
hervorragend. Doch, da sind noch welche bei dir im Keller, in diesem
Marmeladenglas hinten in dem Regal da!
