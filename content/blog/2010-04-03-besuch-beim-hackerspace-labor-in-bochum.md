+++
title = "Besuch beim Hackerspace \"Labor\" in Bochum"
date = 2010-04-03T14:56:31Z
author = "typ_o"
path = "/blog/2010/04/03/besuch-beim-hackerspace-labor-in-bochum"
aliases = ["/blog/archives/81-Besuch-beim-Hackerspace-Labor-in-Bochum.html"]
+++
Download:
[jiffies\_vom\_labor\_bochum.mp3](/media/jiffies_vom_labor_bochum.mp3 "jiffies_vom_labor_bochum.mp3").
Dies ist ein lockeres Gespräch mit dem Laboranten Jiffies, ich fragte
ihn, worauf man so achten könnte, wenn man einen Hackerspace aufbaut.
