+++
title = "Das neue ABX heißt jetzt ZNL"
date = 2010-03-01T20:36:22Z
author = "typ_o"
path = "/blog/2010/03/01/das-neue-abx-heisst-jetzt-znl"
aliases = ["/blog/archives/75-Das-neue-ABX-heisst-jetzt-ZNL.html"]
+++
Unser zukünftiges Gebäude beherbergte früher "ABX-Logistics", und wie
ein Bahner sagte, wäre das die völlig falsche Abkürzung, in Wirklichkeit
sei das Gebäude die "ZNL", **Zentrale Netzleitstelle** der Bahn. Na -
paßt das nicht ganz hervorragend?
