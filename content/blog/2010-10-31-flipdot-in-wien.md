+++
title = "flipdot in Wien"
date = 2010-10-31T05:54:05Z
author = "typ_o"
path = "/blog/2010/10/31/flipdot-in-wien"
aliases = ["/blog/archives/117-flipdot-in-Wien.html"]
+++
[![](/media/biennale_06.serendipityThumb.jpg)](/media/biennale_06.jpg)Relativ
kurz geplant hat sich eine kleine Delegation vom hackerspace kassel nach
Wien aufgemacht. Schön da, sehr entspannte Großstadt, und viel
medienbezogener und nerdkompatibler Content. Unter anderem die [Media
Architecture
Biennale 2010](https://www.mediaarchitecture.org/biennale2010/), also
alles zum Thema "blinkende Häuser". (Sehr viel multimedialer und
interaktiver ist hier noch nichts, ausser blinkende LED).

[![](/media/biennale_05.serendipityThumb.jpg)](/media/biennale_05.jpg)Im
Künstlerhaus am Karlsplatz in Wien gibt es eine Ausstellung dazu. Ganz
ambitioniert gemacht: Jeder Besucher kriegt ein Ipad in die Hand, auf
dem er Details zu den Ausstellungsstücken abrufen kann, im Wesentlichen
den [Content der Übersichtsseite im Web zur
Ausstellung](https://www.mediaarchitecture.org/biennale-2010-exhibition/).
Ging auch meistens, ab und zu hing der Server, und man konnte im Display
gucken, ob man noch frisch aussieht.

[![](/media/biennale_04.serendipityThumb.jpg)](/media/biennale_04.jpg)Was
mir dazu so durch den Kopf ging: Wenn blinkende Fassaden, dann ist ja
[Blinkenlights](https://blinkenlights.net/) erstmal die amtliche Adresse.
Das Projekt war natürlich auch dabei. Alle anderen Häuser haben dann den
Bogen völlig überspannt, und Pixelanzahl und Farbtiefe erhöht, bis man
einfach das Gefühl hat, vor einem großen Fernseher zu stehen.

Vermutlich kann man das grafisch darstellen, mit wieviel Pixeln und
wieviel Farben das Spiel noch cool rüberkommt. 4 Pixel ist zu wenig,
400.000 zu viel. Vermutlich sind die 144 Pixel von Blinkenlights nahe am
Optimum - noch schön abstrakt, und schon so gut aufgelöst, dass man
Inhalt rüberbringen kann.

[![](/media/biennale_01.serendipityThumb.jpg)](/media/biennale_01.jpg)[![](/media/biennale_02.serendipityThumb.jpg)](/media/biennale_02.jpg)Für
flipdot hat mir ein Projekt gut gefallen, in dem einfach weisse
Transportkisten kopfunter gestapelt wurden. ([Hier im
Katalog](https://www.mediaarchitecture.org/boxleds/)) Im Inneren eine
Platine mit LED und etwas Buslogik. Schön groß, schön billig. Bei uns im
Wiki reift ja eine ganz ähnliche Idee mit den [beleuchteten
Kanistern](https://flipdot.org/wiki/index.php?title=Kanister). Auch schön
simpel: [Dieses
Projekt](https://www.mediaarchitecture.org/portable-deflatable-led-matrix-by-werkzeug-h/).
