+++
title = "Workshop Messer schleifen"
date = 2017-04-09T10:49:31Z
author = "typ_o"
path = "/blog/2017/04/09/workshop-messer-schleifen"
aliases = ["/blog/archives/371-Workshop-Messer-schleifen.html"]
+++
[![](/media/messerschleifen.serendipityThumb.jpg)](/media/messerschleifen.jpg)

Vielen Dank an **Mikola** für den Workshop!

Quelle von allerlei Werkzeug, Messern und Schleifsteinen:
[Dictum](https://www.dictum.com/de/)

Zum Schutz vor Rost bei nicht rostfreien Messern in diesem Shop nach
Sinensis Kamelienöl suchen. Das ist geschmacksneutral und
lebensmittelecht.

Zum gerade machen von den Steinen einen Rutscherstein nehmen (auf Größe
beim Kauf achten). Damit der besser greift Siliziumkarbid in Pulverform
drauf streuen (gibts auch bei Dictum).

Der grüne, grobe Stein war ein Naniwa Sharpening Stone 400.

Mir ist aufgefallen, dass eure orangenen Steine nicht so viel Material
abgegeben haben und somit nicht so empfindlich auf "auf der stelle
kratzen" sind. Es gab eine deutliche Kuhle im grünen Stein - da ist mir
klar geworden, dass man bei so einem Stein nicht nur aufs Messer,
sondern auch auf den Stein beim schleifen achten muss und es somit
doppelt viel zu lernen gibt. Vom 1000er, der so weich ist, dass man rein
schneiden kann ganz zu schweigen...
