+++
title = "Workshop: Moderne Website-Layouts mit flexbox"
date = 2017-09-09T13:30:34Z
author = "typ_o"
path = "/blog/2017/09/09/workshop-moderne-website-layouts-mit-flexbox"
aliases = ["/blog/archives/389-Workshop-Moderne-Website-Layouts-mit-flexbox.html"]
+++
Offener Workshop am 2017-09-16 um 17:00 im
[flipdot](/kontakt/). Die Veranstaltung ist für
Leute gedacht, die schon ein gewisses Grundverständnis von HTML + CSS
haben. Laptop mitbringen ist empfohlen, aber nicht notwendig.

<!-- more -->

Übersicht zum Talk:

- Recap: Layouts mit float und position: absolute
- display: flex to the rescue!
- Demo / Übung: Flexbox Froggy
- HowTo: Sticky Footer mit Flexbox

![](/media/boxes2.serendipityThumb.jpg)
