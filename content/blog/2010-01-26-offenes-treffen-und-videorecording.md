+++
title = "Offenes Treffen und Videorecording"
date = 2010-01-26T05:30:25Z
author = "typ_o"
path = "/blog/2010/01/26/offenes-treffen-und-videorecording"
aliases = ["/blog/archives/66-Offenes-Treffen-und-Videorecording.html"]
+++
Heute wieder 19:00 im
[Kunsttempel](https://flipdot.org/blog/archives/47-Ab-jetzt-immer-Dienstags.html).
Topics neben Raumsituation und Innenplanung ein 30sec. Interview mit
jedem der was zum Hackerspace sagen mag, Frage: "Was wird dein erstes
Projekt im Hackerspace sein?".
