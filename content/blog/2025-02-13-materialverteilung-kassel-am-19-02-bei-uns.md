+++
title = "Materialverteilung Kassel am 19.02. bei uns"
date = "2025-02-13T08:02:00.000Z"
author = "soerface"
path = "materialverteilung-februar-2025"
draft = false
+++
Die Materialverteilung Kassel setzt sich für einen nachhaltigen Umgang mit Ressourcen ein und möchte eine Kultur des Teilens und Weitergebens fördern. Anstatt brauchbare Materialien wegzuwerfen, können sie durch Umverteilung neue Verwendung finden – ein Beitrag zu ökologischer Nachhaltigkeit und sozialer Gerechtigkeit.

Diesen Monat findet das offene Treffen der Materialverteilung bei uns statt. Alle Interessierten sind herzlich eingeladen, sich zu informieren, mitzumachen und gemeinsam Ideen für eine ressourcenschonende Zukunft zu entwickeln.

📅 Wann? 19. Februar, 19:00 Uhr

📍 Wo? [Im flipdot](https://flipdot.org/kontakt/#Anfahrt)

Mehr Informationen unter <https://materialverteilung.org>
