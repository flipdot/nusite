+++
title = "Vorbereitung auf das Baustel-Wochenende"
date = 2010-02-16T07:25:43Z
author = "typ_o"
path = "/blog/2010/02/16/vorbereitung-auf-das-baustel-wochenende"
aliases = ["/blog/archives/71-Vorbereitung-auf-das-Baustel-Wochenende.html"]
+++
Heute Abend treffen wir uns wieder im Kunsttempel in Kassel um
**19:00h**. Interessiert? Einfach dazukommen.

Hauptsächlich wird es um die Vorbereitung des Baustel-Wochenendes gehen,
das wir vom 19.2. bis zum 21.2.2010 ausrichten: "Wir wollen ans Gelbe
vom Ei!" - Mikrocontroller von basics bis advanced.

[kunsttempel.net](https://www.kunsttempel.net), und [Google
Maps](https://maps.google.com/maps?f=q&source=s_q&hl=de&geocode=&q=Friedrich-Ebert-Str.+177,+kassel&sll=37.0625,-95.677068&sspn=23.761683,55.810547&ie=UTF8&hq=&hnear=Friedrich-Ebert-Stra%C3%9Fe+177,+West+34119+Kassel,+Hessen,+Deutschland&ll=51.316089,9.460862&spn=0.00057,0.002725&t=h&z=19),
Anreisekoordinaten für Geocacher: 51.316065,9.460679
