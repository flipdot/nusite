+++
title = "flipdot goes 26C3"
date = 2009-12-19T10:06:55Z
author = "typ_o"
path = "/blog/2009/12/19/flipdot-goes-26c3"
aliases = ["/blog/archives/60-flipdot-goes-26C3.html"]
+++
Eine doch größere Teilmenge der flipdots fähr nach Berlin zum
[26C3](https://events.ccc.de/congress/2009/wiki/), wir haben uns
zur Koordination auf dem [IRC-Kanal des
CCC-KS](irc://irc.rizon.net/cccks) verabredet. (Ein guter IRC-Client für
Firefox ist
[Chatzilla](https://addons.mozilla.org/de/firefox/addon/16)).
