+++
title = "Das Auge schließt mit!"
date = 2016-06-02T09:38:04Z
author = "typ_o"
path = "/blog/2016/06/02/das-auge-schliesst-mit"
aliases = ["/blog/archives/345-Das-Auge-schliesst-mit!.html"]
+++
Neue Schlossmechanik für unseren Hackerspace.

[![](/media/schloss3.serendipityThumb.jpg)](/media/schloss3.jpg)

In unseren vorherigen Räumen hatten wir ein
"[Aufsatzschloss](https://www.abus.com/var/ezflow_site/storage/images/media/keyvisuals/sicherheit-zuhause/tuersicherheit/keyvisual-tuer-zusatzschloesser-fehlt-in-keinem-krimi/221161-1-ger-DE/Keyvisual-Tuer-Zusatzschloesser-Fehlt-in-keinem-Krimi_slide_wide.jpg)",
dessen Knebel auf der Innenseite mit einem Motor betätigt wurde.
Aufschließen konnte man elektrisch, und von außen mit dem Schlüssel.

![](/media/IMAG1549.jpg)

Das lief lange Zeit auch gut, bis der Motor einen Endschalter überfahren
hat, und das Schloß in Stellung "Zu" stehen blieb - der Knebel nicht in
waagerechter Lage, sondern am mechanischen Anschlag.  
Wirkung: Der Space war zu, aber auch von außen nicht zu öffnen, da der
Schlüssel nur schließt, wenn der Knebel innen waagerecht steht. Wir sind
dann durchs Fenster in den Space eingebrochen und haben aufgemacht.

Um diese Situation nicht wieder zu haben, und auch einem Strom- oder
Rechnerausfall vorzubeugen, haben wir die Lösung erweitert. Der Riegel
von dem Aufsatzschloß hat jetzt kein festes Gegenstück mehr, sondern
schlägt an einen zweiten Riegel, der motorisch bewegt werden kann: Zwei
völlig separate Möglichkeiten zu öffnen.

[![](/media/installation.serendipityThumb.jpg)](/media/installation.jpg)

Über dem Motorriegel sieht man ein Schiebepoti, damit wird später noch
die Riegelposition kontrolliert. Der Aktuator ist einfach ein
Zentralverriegelungselement aus Autos. DC-Motor plus Zahnstange. Preis
auf dem Flohmarkt: 10 EUR für vier Stück.

![](/media/verriegelung.serendipityThumb.jpg)

Links: Verriegelt, dann elektrisch geöffnet, und rechts manuell
geöffnet.
