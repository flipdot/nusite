+++
title = "Holzlager upcoming"
date = "2024-07-23T19:11:00.000Z"
author = "and…"
path = "/blog/2024/07/23/holzlager_upcoming"
draft = false
+++
## So ein Hackspace braucht Holz, viel Holz!

Da unser Umzug immer noch nicht vollständig abgeschlossen ist und ein großer Teil der Werkstatt aktuell im Chaos versinkt, haben sich ein paar fleißige Hände daran gemacht ein System aufzubauen. Da überall einzeln herumliegenden und nur notdürftig versorgte Holzteile, wenig übersichtlich sind und vor Platz wegnehmen, gibt es bald einen Rahmen, so dass alles fein versorgt, übersichtlich angeordnet ist und man zukünftig einfach das Teil dass man benötigt, finden und einfach zum bearbeiten wegnehmen kann.

![](/media/img_2790.jpeg)

![](/media/img_2791.jpeg)
