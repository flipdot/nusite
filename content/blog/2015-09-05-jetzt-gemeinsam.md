+++
title = "Jetzt gemeinsam!"
date = 2015-09-05T10:24:03Z
author = "typ_o"
path = "/blog/2015/09/05/jetzt-gemeinsam"
aliases = ["/blog/archives/323-Jetzt-gemeinsam!.html"]
+++
Mehrere Artikel in der taz von heute zum Thema Sharing, Open Source,
Offene Wissensquellen

Innovationscamp [Proof of Concept POC21](https://www.poc21.cc/) -
umweltschonende Produkte

[Openstructures](https://openstructures.net), ein Open Hardware
Konstruktionspool für Alltagsgegenstände

[Open Source Circular Economy Days 2015](https://oscedays.org/) ("We
share the vision of a circular economy. An idea for a truly sustainable
future that works without waste")

[Open Source Ecology](https://opensourceecology.de/) Germany ("Wir
verwenden nachhaltige Kriterien für die Entwicklung von Technologien")

[Open Access](https://www.open-access.net/) - freier Zugang
zu wissenschaftlicher Information

[Commonsblog](https://commonsblog.wordpress.com/) ("Fundsachen von der
Allmendewiese") - Vergemeinschaftung von Gütern und Wissen
