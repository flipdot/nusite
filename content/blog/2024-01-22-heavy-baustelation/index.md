+++
title = "heavy baustelations 2024-01-22"
date = 2024-01-22T19:15:00Z
author = "and…"
path = "/blog/2024/01/22/heavy-baustelations"
draft = false
+++

## Am Wochenende wurde heftig gebaustelt.  <br /> 
### Ein besonderes Highlight ist jetzt unsere Sofa-Lounge. Einige unserer Member haben sich ein Herz gefasst und die Arbeitsplatte, die vorher einfach auf Eurokisten lag und so jahrelang als Tisch diente, nun mit einem Tischgestell versehen, selbst gebaut selbstverständlich! In die Mitte unter den Tisch eine Stromverteilerdose gelegt und dann Kabel nach außen gezogen, so dass jetzt an allen Seiten Mehrfachsteckdosen sind.  <br /> Damit können nun alle lümmelwillige Member, die in der Sofa-Ecke sitzen, easy ihre stromhungrigen Geräte dort einstöpseln, ohne das großes Kabelgewurschtel oder Stolperfalle entstehen. 
### Cool, oder?  <br /> 
### Auch unser Soundsystem hat jetzt ein feines Regal spendiert bekommen und der Beamer hängt. Wir haben ihn dann direkt mit Film schauen eingeweiht.  <br /> 
## Hast Du Lust Dir das ganze anzuschauen? Komm vorbei, nächsten Dienstag, ab 19h öffnen wir für alle die Pforten, komm rum!
### vorher-Bild: ![so war das vorher](IMG20240121155126.jpg)<br />
### baustelation in progress ![Gewindestangen werden mit einer Flex zugeschnitten](ima_a10d4a4.jpeg)<br />
### Nach dem gebaustel ist's gleich viel netter anzusehen: ![Nach dem gebaustel ist's gleich viel netter anzusehen](2024-01-21-17-10-22-627.jpg) <br />
### Die angebrachten Mehrfachsteckdosen am fertigen Tisch: ![Die angebrachten Mehrfachsteckdosen am fertigen Tisch](2024-01-21-17-10-37-574.jpg) <br />
### Das fertige Regal mit dem Soundsystem: ![Das fertige Regal mit dem Soundsystem](2024-01-21-17-31-44-849.jpg)<br />
