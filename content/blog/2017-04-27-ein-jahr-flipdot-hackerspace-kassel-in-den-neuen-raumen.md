+++
title = "Ein Jahr flipdot hackerspace kassel in den neuen Räumen"
date = 2017-04-27T13:26:55Z
author = "typ_o"
path = "/blog/2017/04/27/ein-jahr-flipdot-hackerspace-kassel-in-den-neuen-raumen"
aliases = ["/blog/archives/376-Ein-Jahr-flipdot-hackerspace-kassel-in-den-neuen-Raeumen.html"]
+++
**flipdot hackerspace kassel:** 2009 gegründet, 2016 in neue Räume im
Kulturbahnhof Kassel eingezogen. Ein Einblick in Werkstätten, Lounge,
Hackplätze, Küche, Kino- und Vortragsraum. Spaß am Gerät im
CCC-ERFA-Kassel flipdot e.V. - gemeinsam löten, fräsen, hacken, coden,
bausteln, diskutieren, lernen, kochen und essen. Jeden Dienstag offen
für Gäste und Besucher: [Komm
vorbei](/kontakt/)!
