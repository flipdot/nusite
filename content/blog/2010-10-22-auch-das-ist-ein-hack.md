+++
title = "Auch das ist ein Hack..."
date = 2010-10-22T20:11:13Z
author = "Fabian"
path = "/blog/2010/10/22/auch-das-ist-ein-hack"
aliases = ["/blog/archives/115-Auch-das-ist-ein-Hack....html"]
+++
Um den Begriff des Hackens richtig zu definieren, bieten sich oft
Beispiele fernab von elektronischen Geräten an.  
Ich dachte z.B. immer, dass das Binden von Schnürsenkeln ein gelöstes
Problem ist. Schließlich machen das doch jeden Tag auf Neue zig
Millionen Leute. Wenn es da etwas zu optimieren gäbe, müsste das doch
jemandem aufgefallen sein. Aber weit gefehlt. Letztens musste ich beim
hören des
[NSFW-Podcast](https://tim.geekheim.de/category/podcast/nsfw/ "NSFW-Podcast")belehren
lassen, dass man nie auslernt.  
Der Australier [Ian Fieggen](https://www.fieggen.com/ "Ian Fieggen") hat
sich ausgiebig mit dem Thema befasst und dokumentiert seine Erfindungen
auf einer speziellen Website. Komplett mit iPhone/iPad-App. Nur über das
Binden und Knoten von Schnürsenkeln... Thats f\*\*\*\*ng awsome!  
Besonders hervorzuheben ist der [Ian Knot (Ian's Fast Shoelace
Knot)](https://www.fieggen.com/shoelace/ianknot.htm "Ian Knot")  
![](https://www.fieggen.com/shoelace/IanKnot16.gif)  
Diese grandiose Erfindung spart mir jeden Tag mit Sicherheit 2-3
Sekunden meiner Lebenszeit. Das sind ca. 18 Minuten im Jahr und bei ein
geschätzen Restlebenszeit von 60 Jahren ca. 18 Stunden zusätzliche
Lebenszeit. Danke Ian!
