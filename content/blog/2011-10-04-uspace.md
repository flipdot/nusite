+++
title = "µSpace"
date = 2011-10-04T21:06:05Z
author = "typ_o"
path = "/blog/2011/10/04/uspace"
aliases = ["/blog/archives/147-Space.html"]
+++
So, es geht voran. Nach unserer Raumkündigung wegen dieser Tangoschule
konnten wir irgendwann das Thema Raumsuche nicht mehr hören, jetzt
können wir aber einen Haken dran machen. Wir haben zwei Räume direkt
neben dem schon mal in Betracht gezogenen Keller für insgesamt 70 EUR
incl. Nebenkosten - da kann man wirklich nichts mehr sagen. Lage in der
Sickingenstraße wie bisher, gleich rechts im Torbogen.

Besichtigung heute =\> Pläne für Renovierung, und dann behutsame
Ausstattung mit möglichst wenigen von den vielen Möbeln die wir haben.
Ich glaube, das ist schlicht genau das passende für unsere jetztige
Größe und Finanzkraft, Erweiterungsmöglichkeit eingeschlossen.
