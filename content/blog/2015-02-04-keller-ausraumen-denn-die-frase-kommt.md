+++
title = "Keller ausräumen - denn die Fräse kommt!"
date = 2015-02-04T07:51:32Z
author = "typ_o"
path = "/blog/2015/02/04/keller-ausraumen-denn-die-frase-kommt"
aliases = ["/blog/archives/271-Keller-ausraeumen-denn-die-Fraese-kommt!.html"]
+++
Am Samstag, [2015-02-07](https://xkcd.com/1179/), 10:00, räumen wir
unseren Keller aus - wer Dinge vor dem Schrott retten will, ist
willkommen!

![](/media/eschrott.jpg)

(Picture related) By Thousandways at de.wikipedia \[[CC BY-SA 2.0
de](https://creativecommons.org/licenses/by-sa/2.0/de/deed.en)\], [via
Wikimedia
Commons](https://commons.wikimedia.org/wiki/File%3AEschrott-brd.jpg)
