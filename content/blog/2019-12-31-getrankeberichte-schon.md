+++
title = "Getränkeberichte, schön"
date = 2019-12-31T17:44:00Z
author = "typ_o"
path = "/blog/2019/12/31/getrankeberichte-schon"
aliases = ["/blog/archives/432-Getraenkeberichte,-schoen.html"]
+++
Unser Getränkescanner kann per E-Mail Berichte über die zueletzt
gebuchten Getränke und Geldbeträge versenden. Seit einiger Zeit tut er
dies nun hübscher als vorher. Während die E-Mails ehemals nur aus Text
bestanden...

[![](/media/mail-text.serendipityThumb.jpeg)](/media/mail-text.jpeg)

... unterstützen die E-Mails jetzt auch hübsches HTML & CSS:

[![](/media/mail-html.serendipityThumb.jpeg)](/media/mail-html.jpeg)

Der zugehörige Code findet sich [auf
GitHub](https://github.com/flipdot/drinks-touch/blob/develop/drinks_touch/notifications/notification.py).
