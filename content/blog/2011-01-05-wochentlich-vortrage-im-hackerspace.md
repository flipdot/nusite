+++
title = "Wöchentlich Vorträge im Hackerspace"
date = 2011-01-05T18:00:47Z
author = "Sebastian Löser"
path = "/blog/2011/01/05/wochentlich-vortrage-im-hackerspace"
aliases = ["/blog/archives/124-Woechentlich-Vortraege-im-Hackerspace.html"]
+++
Hi,  
wir fangen endlich an! Ab dem 25.01.2011 wollen wir jeden Di um
ungefähr 20:00 einen Vortrag anbieten.  
Den Anfang macht Dominic(Ric0) am 25.01.2011 mit "Einführung in die
Programmiersprache C".  
Ich(Dino0815) rede dann am 01.02.2011 über "Einführung in die
Programmierung von AVR's mit C"

Es sei jeder aufgerufen zu den entsprechenden Terminen zu erscheinen um
dem jeweiligen Vortrag zu lauschen und sich mit einem beliebigen Thema
für weitere Termine anzumelden.

Beste Grüße,  
Dino0815
