+++
title = "Wegnehmen kann jeder - Hinzufügen können wir (Additive Fertigung)"
date = 2018-01-19T09:36:53Z
author = "Baustel"
path = "/blog/2018/01/19/wegnehmen-kann-jeder-hinzufugen-konnen-wir-additive-fertigung"
aliases = ["/blog/archives/396-Wegnehmen-kann-jeder-Hinzufuegen-koennen-wir-Additive-Fertigung.html"]
+++
3-D Druck im flipdot

[![](/media/IMG_20180117_12263901.serendipityThumb.jpg)](/media/IMG_20180117_12263901.jpg)

Der flipdot hat auch einen 3-D Drucker zu bieten.  
Er kann größere Teile bis ca 210x210x210 mm drucken.

Im Bild ein Teil eines Selbstbau-Wärmetauschers.  
https://ytec3d.com/3dp-heat-exchanger/
