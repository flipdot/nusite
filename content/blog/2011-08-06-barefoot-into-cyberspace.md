+++
title = "Barefoot into Cyberspace"
date = 2011-08-06T05:39:50Z
author = "typ_o"
path = "/blog/2011/08/06/barefoot-into-cyberspace"
aliases = ["/blog/archives/142-Barefoot-into-Cyberspace.html"]
+++
Barefoot into Cyberspace: Adventures in search of techno-Utopia by Becky
Hogge. [Download here.](https://barefootintocyberspace.com/book/)
