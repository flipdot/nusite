+++
title = "Versorgung für Kohlenstoffeinheiten"
date = 2010-08-08T14:05:28Z
author = "typ_o"
path = "/blog/2010/08/08/versorgung-fur-kohlenstoffeinheiten"
aliases = ["/blog/archives/101-Versorgung-fuer-Kohlenstoffeinheiten.html"]
+++
[![](/media/bar.serendipityThumb.jpg)](/media/bar.jpg)Küche
und Bar entstehen gerade, Platten sind zugeschnitten und ein erstes Mal
geölt. Montiert werden die Platten auf waagerecht gelegten Schränken mit
Schließfächern, die ihre Funktion behalten.
