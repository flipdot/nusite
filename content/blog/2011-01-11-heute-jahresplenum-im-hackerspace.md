+++
title = "Heute Jahresplenum im Hackerspace"
date = 2011-01-11T09:58:57Z
author = "typ_o"
path = "/blog/2011/01/11/heute-jahresplenum-im-hackerspace"
aliases = ["/blog/archives/125-Heute-Jahresplenum-im-Hackerspace.html"]
+++
Heute 19:00 treffen wir uns im
[Hackerpace](/kontakt/#Anfahrt) zur
jährlichen Mitgliederversammlung. Gäste sind nachdrücklich eingeladen -
wir sprechen über die Zukunft von flipdot.
