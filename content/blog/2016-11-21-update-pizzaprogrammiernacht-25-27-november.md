+++
title = "Update - Pizzaprogrammiernacht 25. - 27. November"
date = 2016-11-21T14:49:28Z
author = "typ_o"
path = "/blog/2016/11/21/update-pizzaprogrammiernacht-25-27-november"
aliases = ["/blog/archives/358-Update-Pizzaprogrammiernacht-25.-27.-November.html"]
+++
Hallo $Besucher der PPN!

Hier noch ein paar Infos zur PizzaProgrammierNacht:

[Fahrplan](https://flipdot.org/wiki/PPN/Fahrplan) ist online!

Wir planen einen kleinen freiwilligen Show & Tell Part. Ohne Bühne und
Mikro, einfach 30 Sekunden darüber, was dich interessiert, was du gerade
so machst. Keine Slides, keine Vorbereitung - einfach ein paar Worte:
Ich bin bierbrauender Pentester mit Interesse an einer Flash-VM in PHP,
oder so.

Wenn du Interesse hast, einen kleinen Talk zu geben - umso besser! Es
gibt auch Lightning Talks! Lass uns das wissen!
\[com\*aet\*flipdot.org\]

Aufpassen beim Parken: Die Parkplätze gehören der Bahn und sind
(moderat) gebührenpflichtig!

Brauchst du einen Schlafplatz? Bitte vorher Mail an die Orga
\[com\*aet\*flipdot.org\]

Bringt euch Mehrfachsteckdosen und Patchkabel mit!

Es wird zwei Pizza-Backphasen geben (Freitag und Samstag Abend). In der
übrigen Zeit gibt es auf Vorrat gebackene Pizza und einen 24 h -
Frühstückstresen.

Öffnungszeit nach Bedarf 24 h, spätestens aber Samstag und Sonntag
morgen ab 10:00 Uhr

Telefonnummer im Space ist: 0561-47395848

Anfahrt: Kassel Hauptbahnhof, auch bekannt als "Kulturbahnhof Kassel"
(NICHT Bahnhof Wilhelmshöhe). Der Space liegt auf der Südseite vom
Bahnhof - wenn ihr ankommt, fahrt ihr links vom Bahnhof auf einen Parkplatz zu.
\[[1](https://www.openstreetmap.org/node/1716494567/)\]\[[2](https://www.google.de/maps/place/flipdot+hackerspace+kassel/@51.318212,9.4842858,19z/2)\]

Die Anflugschneise ist durch ein blaues Leuchtfeuer
\[[3](https://de.wikipedia.org/wiki/Morsezeichen)\] gekennzeichnet.

Wir wollen wieder ein wenig die Atmosphäre auf Video einfangen, so wie
letztes Mal \[[4](https://vimeo.com/128079323)\]. Dazu wird es zwei
Farben von Badges geben. Wenn du nichts dagegen hast, dass du marginal
im Bild auftauchst, nimmst du dir ein grünes Badge.

[Die Musik zur PPN](https://www.youtube.com/watch?v=ouYB3Ef2qYI7)!

Wir freuen uns schon, kommt und habt Spaß!

PS: Direkt neben flipdot findet gleichzeitig das Randfilm Festival
statt:

> "Das [Randfilmfest 2016](https://www.randfilm.de/)
> 
> Jenseits von Genres und Schubladen - beim Randfilmfest mischen sich
> Film- und Musikevents, treffen Stargäste in familiärer Atmosphäre auf
> ihr Publikum, wird getanzt, gefeiert und diskutiert.
> 
> Spiel-, Dokumentar-, Lang- und Kurzfilme, Horror, Thriller,
> Sozialdrama, Coming-of-Age, Exploitation, Sex, Sleaze, Anime und
> Kunstfilm stehen bei uns gleichberechtigt nebeneinander. Anspruch,
> Unterhaltung, Fun und Verstörung gehören zusammen.
> 
> Ein Hurra auf die Vielfalt des künstlerischen Ausdruck"

Also - wer sich das zusätzlich noch geben mag ... ;)
