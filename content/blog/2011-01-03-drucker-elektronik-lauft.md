+++
title = "Drucker-Elektronik läuft"
date = 2011-01-03T17:26:08Z
author = "typ_o"
path = "/blog/2011/01/03/drucker-elektronik-lauft"
aliases = ["/blog/archives/123-Drucker-Elektronik-laeuft.html"]
+++
[![](/media/IMG_8698.serendipityThumb.jpg)](/media/IMG_8698.jpg)Motor-
und Servotreiber sind fertig, kleinere Probleme mit der Stromversorgung
(Störungen und Verlustleistung) werden heute noch gelöst, die
Controllersoftware steht als frühe Betaversion. Morgen beim
flipdot-Treffen erster Test der Mechanik für X-, Y- und Z-Achse: Wir
malen einzelne Pixel! Wer als Softwerker was beitragen möchte und sich
der Fragestellung widmen möchte "Bitmap öffnen und Farbwerte über die
serielle Schnittstelle einzeln rausschicken" - herzlich willkommen im
Team!
