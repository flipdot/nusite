+++
title = "Pygame zum anfassen - 11. Juni 2016, 17:00"
date = 2016-06-09T18:22:44Z
author = "typ_o"
path = "/blog/2016/06/09/pygame-zum-anfassen-11-juni-2016-17-00"
aliases = ["/blog/archives/347-Pygame-zum-anfassen-11.-Juni-2016,-1700.html"]
+++
![](/media/pygame.serendipityThumb.png)

Pygame ist ein von Pete Shinners entwickeltes, der Spieleprogrammierung
dienendes Set von plattformübergreifenden Python-Modulen
\[[Q](https://de.wikipedia.org/wiki/Pygame)\], das sich aber auch ganz
hervorragend zum Aufbau von Benutzeroberflächen verwenden läßt.

Dies ist kein Vortrag oder strukturierter Workshop, sondern ein
gemeinsames hands on. Jeder ist willkommen, Eintritt frei. Wo?
[flipdot](/kontakt/).
