+++
title = "Beteiligung von flipdot auf dem Galeriefest in Kassel"
date = 2018-05-26T11:28:29Z
author = "typ_o"
path = "/blog/2018/05/26/beteiligung-von-flipdot-auf-dem-galeriefest-in-kassel"
aliases = ["/blog/archives/407-Beteiligung-von-flipdot-auf-dem-Galeriefest-in-Kassel.html"]
+++
Mehrere flipdot Member haben an zwei Installationen auf dem [Galeriefest
Kassel 2018](https://galerien-kassel.de/) gearbeitet.

Der Realitätstranslator läßt dich in ein Paralleluniversum reisen, wo du
den Platz mit deinem Doppelgänger tauscht und du dich über all die
merkwürdigen Menschen dort wundern kannst - allesamt Doppelgänger!

![](/media/translator1.serendipityThumb.jpg)

Die Shepard-Ton-Installation erzeugt einen örtlich wandernden Akkord,
der unentwegt an Tonhöhe zunimmt.

![](/media/shepard.serendipityThumb.jpg)
