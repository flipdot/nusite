+++
title = "Kleiner Arduino Roboter zum nachbauen."
date = 2013-03-07T16:09:59Z
author = "Aleks"
path = "/blog/2013/03/07/kleiner-arduino-roboter-zum-nachbauen"
aliases = ["/blog/archives/184-Kleiner-Arduino-Roboter-zum-nachbauen..html"]
+++
Keine Kenntnisse in Programmierung, Elektronik und Mechanik aber schon
immer davon geträumt einen eigenen kleinen Roboter zu bauen?  
Kein Problem!!! :)

[Anleitung hier
:)](https://5volt-junkie.net/arduino-roboter-selber-bauen/ "https://5volt-junkie.net/arduino-roboter-selber-bauen/")
