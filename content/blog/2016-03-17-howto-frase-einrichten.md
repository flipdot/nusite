+++
title = "HowTo: Fräse einrichten"
date = 2016-03-17T11:57:21Z
author = "typ_o"
path = "/blog/2016/03/17/howto-frase-einrichten"
aliases = ["/blog/archives/336-HowTo-Fraese-einrichten.html"]
+++

Eine kleine Anleitung, wie in unserer Werkstatt die Fräse eingerichtet wird.
Im Wiki gibt es noch [einen detaillierten Text](/projekte/cnc-fräse/how-to/) dazu.
