+++
title = "Spannpratzen"
date = 2015-07-14T17:49:03Z
author = "flipdot member"
path = "/blog/2015/07/14/spannpratzen"
aliases = ["/blog/archives/312-Spannpratzen.html"]
+++
[![](/media/20150714_174334.serendipityThumb.jpg)](/media/20150714_174334.jpg)

[![](/media/20150715_183624.serendipityThumb.jpg)](/media/20150715_183624.jpg)

3 mm HSS Fräser, ca 6000 upm, Zustellung 1 mm, Vorschub 400 mm / min.
Kühlung: Wasser mit Pinsel aufgetragen.  
Gelernt: Alu-Guß läßt sich gut zerspannen, kurze Späne ähnlich wie
Messing.
