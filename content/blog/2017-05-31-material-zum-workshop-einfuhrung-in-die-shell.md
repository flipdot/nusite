+++
title = "Material zum Workshop: Einführung in die Shell"
date = 2017-05-31T17:36:03Z
author = "typ_o"
path = "/blog/2017/05/31/material-zum-workshop-einfuhrung-in-die-shell"
aliases = ["/blog/archives/381-Material-zum-Workshop-Einfuehrung-in-die-Shell.html"]
+++
Hier noch von unserer letzten Veranstaltung "Was Sie schon immer über
Technik wissen wollten..." die Materialien zu "Workshop: Einführung in
die Shell" (Wurde wegen Krankheit am 30.05.2017 nachgeholt).

[talk-shell-introduction.md](/media/talk-shell-introduction.md "talk-shell-introduction.md")

Eine Menge Praxisbeispiele, Shortcuts und nützliche Tips. Von Feliks -
Raum 4 war so voll, dass man auf dem Flur sitzen musste! Super Talk, kthx!
