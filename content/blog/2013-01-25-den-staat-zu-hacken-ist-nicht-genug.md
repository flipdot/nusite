+++
title = "Den Staat zu hacken ist nicht genug"
date = 2013-01-25T08:10:25Z
author = "typ_o"
path = "/blog/2013/01/25/den-staat-zu-hacken-ist-nicht-genug"
aliases = ["/blog/archives/181-Den-Staat-zu-hacken-ist-nicht-genug.html"]
+++
Michael Seemann in der Zeit: [Staatstrojaner zu hacken, genügt nicht,
denn es macht sie
besser.](https://www.zeit.de/digital/internet/2013-01/hacktivismus-ccc)  
Der Hacktivismus ist erfolgreich und gerade weil er erfolgreich ist,
läuft er Gefahr, im Erfolg zu erstarren, staatstragend,
institutionalisiert, professionalisiert und schließlich vereinnahmt zu
werden. Der CCC nimmt immer mehr die Rolle des Digital-Tüv für
bunderepublikanische Angelegenheiten ein und wird damit zum Teil des
Systems. Ein Rädchen im Uhrwerk der Maschine, die Trojanergesetze
ausspuckt und darauf achtet, stets seine Überwachungssoftware zu
verbessern.
