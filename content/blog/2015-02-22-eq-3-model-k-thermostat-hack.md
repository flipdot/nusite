+++
title = "eQ-3 Model K Thermostat Hack"
date = 2015-02-22T11:49:56Z
author = "typ_o"
path = "/blog/2015/02/22/eq-3-model-k-thermostat-hack"
aliases = ["/blog/archives/274-eQ-3-Model-K-Thermostat-Hack.html"]
+++
[Thermostat eQ-3 Model K Hack](https://vimeo.com/120285842) from
[flipdot: hackerspace kassel](https://vimeo.com/flipdot) on
[Vimeo](https://vimeo.com).

Der elektronische Heizkörperthermostat eQ-3 Model K (Ähnlich:
[Reichelt](https://www.reichelt.de/Regler-Thermostate/EHT-CLASSIC-PRO/3/index.html?ACTION=3&GROUPID=4388&ARTICLE=102691&SEARCH=eQ-3%20Model%20K&OFFSET=16&WKID=0&))
lässt sich in ein paar Minuten auf elektronische Betätigung umrüsten, um
ihn mit einem Controller oder einem Raspberry Pi anzusteuern.

In diesem Fall ist das ein CAN Client mit einem AVR Controller drin, der
über den CAN Bus von einem Pi angesteuert wird.
