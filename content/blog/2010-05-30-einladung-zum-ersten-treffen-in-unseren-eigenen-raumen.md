+++
title = "Einladung zum ersten Treffen in unseren eigenen Räumen!"
date = 2010-05-30T17:38:30Z
author = "typ_o"
path = "/blog/2010/05/30/einladung-zum-ersten-treffen-in-unseren-eigenen-raumen"
aliases = ["/blog/archives/90-Einladung-zum-ersten-Treffen-in-unseren-eigenen-Raeumen!.html"]
+++
Am Dienstag, dem 1. Juni treffen wir uns in unseren eigenen Räumen in
der
[Sickingenstraße 10](https://maps.google.com/maps?f=q&source=s_q&hl=de&geocode=&q=sickingenstrasse+10,+kassel&sll=51.320193,9.495353&sspn=0.001224,0.005493&g=51.320238,9.495471&ie=UTF8&hq=&hnear=Sickingenstra%C3%9Fe+10,+Kassel+34117+Kassel,+Hessen,+Deutschland&ll=51.320465,9.495202&spn=0.001224,0.005493&z=18)
um 19:00 Uhr. Neu dabei? Interessiert mitzumachen? Kommt alle!
