+++
title = "Workshops zum Range Sound Festival"
date = "2024-10-22T12:00:00.000Z"
author = "typ_o"
path = "/blog/2024/10/22/Workshops_zum_Range_Soundfestival"
draft = false
+++
Zum [Range Sound Festival](https://range-festival.de/) vom 18. bis 20. Oktober 2024 im Schillerviertel in Kassel war der flipdot hackspace kassel Unterstützer und Veranstaltungsort. Flipdot member haben im Space zwei Workshops gegeben und in den Austellungsräumen der nachrichtenmeisterei Installationen gezeigt.

![Workshop FALL1NG BRA2CHES](/media/range000olaf.jpg "Workshop FALL1NG BRA2CHES")

Workshop FALL1NG BRA2CHES

Im Workshop FALL1NG BRA2CHES von flipdot Member der ersten Stunde [Olaf Val](https://projects.olafval.de/) wurden kleine mechanische Rhythmusmaschinen aus den Fundstücken gebaut, die bei einem Spaziergang gesammelt wurden. Die Fundstücke wurden an Elektromotoren montiert und mit einer Arduino-Steuerung verkabelt. Anschließend konnte mit der Programmierung von Rhythmen und Klängen experimentiert werden.



![Workshop The Hidden Symphony](/media/range001.png "Workshop The Hidden Symphony")

Workshop The Hidden Symphony



Der Workshop The Hidden Symphony von den flipdot Membern [Eeva Ojanperä](https://www.eevaojanperae.de/) und typ_o hatte zum Ziel, mit den Teilnehmer:innen Sensoren für elektrische und magnetische Felder aus Draht, Metallfolie und elektronischen Komponenten zu bauen, und dann auf die Jagd nach den hörbar gemachten elektromagnetischen Feldern im Hackspace zu gehen.

![Werkschau Im Dazwischen](/media/range002.png "Werkschau Im Dazwischen")

Werkschau Im Dazwischen



[Eeva Ojanperä](https://www.eevaojanperae.de/) zeigte die Werkschau Im Dazwischen: Greifbare und un(be)greifbare Begebenheiten wandeln durch diesen Raum wie Phänomene einer anderen Dimension. Fragen tun sich auf: Schaue ich auf den Himmel oder unter ihn? Wo befinde ich mich? Im empfindsamen Schwellenzustand schwinden Grenzen in- und auseinander. Innen und Außen verlieren an Gestalt.



![Installation Blinkenglow](/media/range003.png "Installation Blinkenglow")

Installation Blinkenglow



Die Installation Blinkenglow von typ_o visualisiert Zufall. In der Kryptographie benötigt man möglichst zufällige Zahlenreihen - Kernzerfall ist als quantenphysikalischer Effekt völlig zufällig. Ein Geigerzähler triggert Elemente einer Lichtinstallation. Die elektronische gesteuerte Helligkeitsveränderung wird hörbar gemacht und erzeugt schwebende Klänge.
