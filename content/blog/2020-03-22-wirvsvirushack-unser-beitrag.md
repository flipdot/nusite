+++
title = "#WirVsVirusHack: Unser Beitrag"
date = 2020-03-22T19:03:23Z
author = "Baustel"
path = "/blog/2020/03/22/wirvsvirushack-unser-beitrag"
aliases = ["/blog/archives/441-WirVsVirusHack-Unser-Beitrag.html"]
+++
Wir haben beim Hackathon der Bundesregierung gegen Corona teilgenommen.
Schaut doch gerne bei YouTube vorbei und lasst uns einen Daumen da:
<https://www.youtube.com/watch?v=3uElhbFbeVY>

- [Projekt auf Devpost](https://devpost.com/software/flipshare)
- [Mockup auf Figma](https://figma.com/file/dd80C92aAhPiuSvsKSdVO1/)
- [Projekt auf GitHub](https://github.com/flipdotvsvirus)
- [Video bei YouTube](https://www.youtube.com/watch?v=3uElhbFbeVY)
