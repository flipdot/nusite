+++
title = "Wir sind umgezogen"
date = 2024-01-02T11:29:35.722Z
author = "and…"
path = "/blog/2024/01/02/umzug"
draft = false
+++

Schweren Herzens haben wir die Kündigung der Deutschen Bahn zur Kenntnis genommen. Zum ersten Januar mussten wir unseren schönen alten Space verlassen haben. Viele Jahre hatten wir uns hinter dem Bahnhof, in der Nachrichtenmeisterei eingenistet und dort schöne Erlebnisse gesammelt. Wie das aber so ist, kam dort irgendwann unsere Zeit und wir mussten weichen, alles einpacken und uns eine neue Bleibe suchen. (unter anderem deswegen war es hier so lange still)

Wie es zuletzt hieß, *"vor Computer kommt Chaos"* und da wir uns treu vor diesem Motto verbeugen, haben wir jetzt viel Chaos, aber eben in neuen Räumen!

Wir sind jetzt in der [Schillerstraße 25](https://routing.openstreetmap.de/?z=18&center=51.320863%2C9.495055&loc=51.320863%2C9.495052&hl=de&alt=0&srv=2) und haben ab sofort wieder total Cyber-mäßig jeden Dienstag die Pforten weit offen für alle. Wir klären aktuell den temporären Zusatz "Cybermuseum und ChaosDocumenta" aufzunehmen, das müssen wir aber noch in den nächsten Plena besprechen. Spaß!

Kommt rum wenn Ihr Lust und Zeit habt, besucht uns, wagt einen Rundgang und lümmelt euch in der Lounge. Wir sind wieder da!

Hier noch ein paar Impressionen

![](/media/img_1986.jpg)

![](/media/img_1952.jpg)

![](/media/img_1987.jpg)

![](/media/img_1981.jpg)