+++
title = "Frickel - Punk"
date = 2009-10-12T01:00:00Z
author = "typ_o"
path = "/blog/2009/10/12/frickel-punk"
aliases = ["/blog/archives/40-Frickel-Punk.html"]
+++
Diese durchgeknallten Bastler sind einfach nur Punker. Das war kein
"bisschen Knallen", was die da gemacht haben. Wenn man einen
Werkstattwagen aus MDF baut, kühlschrankgroße Pakete aus hunderten
Kondesatoren darauf parallelschaltet, eine controllerbestückte
Fernzündung betätigt, die bewirkt, daß sich die zwischen den Elektroden
befindliche M10-Schraube in einem 1,5 Kilovolt-Funkenregen auflöst -
dann knallt das nich nur so ein bisschen. Wirklich nicht.

Wenn man in den Abgasstrom des Motors einer Kettensäge einen wie eine
Trompete geformten Schallkanal statt eines Schalldämpfers anbringt, wenn
man diesen nicht wie normal in einen Auspuff münden läßt, sondern
stattdessen einen Schalltrichter wie bei einem Megafon anbringt - dann
"knattert" das nicht. Nein, Wirklich nicht.

Und wenn man den Antennendraht für den neben dem schon aktiven
UKW-Rundfunksender außerdem geplanten Kurzwellensender in den Baum
bringen will - dann macht man das nicht mit einer Zwille, sondern mit
einer Bazooka - ähnlichen Vorrichtung, am Kompressor aufgeladen, mit
Zielfernrohr und pneumatischem Auslöser. (Die Videos entstanden auf dem
[Treffen der Fans](https://www.fingers-welt.de/jubilaeum/treffen2009.htm)
von [Fingers elektrischer Welt](https://fingers-welt.de/), der amtlichen
Bastelseite schlechthin. Tasse kaffee machen, zurücklehnen, und alle
Projektdokus angucken)
