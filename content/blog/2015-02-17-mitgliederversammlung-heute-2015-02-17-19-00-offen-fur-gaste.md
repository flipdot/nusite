+++
title = "Mitgliederversammlung heute, 2015-02-17, 19:00. Offen für Gäste"
date = 2015-02-17T12:08:49Z
author = "typ_o"
path = "/blog/2015/02/17/mitgliederversammlung-heute-2015-02-17-19-00-offen-fur-gaste"
aliases = ["/blog/archives/273-Mitgliederversammlung-heute,-2015-02-17,-1900.-Offen-fuer-Gaeste.html"]
+++
19:00 Uhr im Restaurant Arkadas direkt bei den flipdot - Räumen in der
Sickingenstrasse 10 (Innenhof), 34117 Kassel. Wir freuen uns über
Gäste!
