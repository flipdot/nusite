+++
title = "Sicherheit für Fahrradfahrer in Kassel"
date = 2018-11-21T02:02:59Z
author = "Baustel"
path = "/blog/2018/11/21/sicherheit-fur-fahrradfahrer-in-kassel"
aliases = ["/blog/archives/421-Sicherheit-fuer-Fahrradfahrer-in-Kassel.html"]
+++
Treffen am Sonntag, den 25.11.2018, 16 Uhr.

Auf dem I²oT Hackathon der Universität Kassel haben wir begonnen, einen
Open-Source-Klon des [Radmesser-Projektes in
Berlin](https://interaktiv.tagesspiegel.de/radmesser/index.html) zu
entwickeln.

Ziel des Projektes ist, zu messen, in welchem Abstand Fahrradfahrer an
welchen Orten in der Stadt überholt werden. Durch Auswertung dieser
Daten können Gefahrenstellen erkannt, und Vorschläge zur Beseitigung
gemacht werden.  
Dies könnten beispielsweise neue Radwege, andere Verkehrsführungen oder
angepasste Ampelschaltungen sein.

Bereits realisiert wurde ein rudimentärer Prototyp mit zwei
Ultraschallsensoren, die den Abstand zu Autos messen können, und deren
Rohdaten auf einer SD-Karte gespeichert werden.  
Das Repository ist unter
[https://github.com/flipdot/wheelknife/](https://github.com/flipdot/wheelknife/ "https://github.com/flipdot/wheelknife/")
zu finden, erste Messdaten unter
[https://github.com/flipdot/wheelknife-data/](https://github.com/flipdot/wheelknife-data/ "https://github.com/flipdot/wheelknife-data/").

Am Sonntag soll es darum gehen, den weiteren Verlauf des Projektes zu
planen und am Prototypen weiter zu bausteln. Interessierte sind
eingeladen, am Sonntag vorbeizukommen.
