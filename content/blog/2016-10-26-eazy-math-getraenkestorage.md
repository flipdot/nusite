+++
title = "Eazy math / Getränkestorage"
date = 2016-10-26T14:23:34Z
author = "typ_o"
path = "/blog/2016/10/26/eazy-math-getraenkestorage"
aliases = ["/blog/archives/355-Eazy-math-Getraenkestorage.html"]
+++
Ein Haufen Kisten in der Ecke, halb voll, halb leer? Das geht besser,
wir haben eine CNC Fräse!

[![](/media/DSC_6639.serendipityThumb.JPG)](/media/DSC_6639.JPG)

[![](/media/DSC_6653.serendipityThumb.JPG)](/media/DSC_6653.JPG)

[![](/media/DSC_6637.serendipityThumb.JPG)](/media/DSC_6637.JPG)

[![](/media/DSC_6641.serendipityThumb.JPG)](/media/DSC_6641.JPG)

[![](/media/DSC_6642.serendipityThumb.JPG)](/media/DSC_6642.JPG)

[![](/media/DSC_6657.serendipityThumb.JPG)](/media/DSC_6657.JPG)

**x 5 x 2 + \'n paar Schrauben =**

[![](/media/DSC_6658.serendipityThumb.JPG)](/media/DSC_6658.JPG)

[![](/media/regal_v2.serendipityThumb.png)](/media/regal_v2.png)

Eazy math / Getränkestorage
