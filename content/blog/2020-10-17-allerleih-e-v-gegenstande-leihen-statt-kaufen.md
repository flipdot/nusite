+++
title = "allerleih e.V. - Gegenstände leihen statt kaufen"
date = 2020-10-17T12:42:26Z
author = "Baustel"
path = "/blog/2020/10/17/allerleih-e-v-gegenstande-leihen-statt-kaufen"
aliases = ["/blog/archives/446-allerleih-e.V.-Gegenstaende-leihen-statt-kaufen.html"]
+++
In unserer Stadt hat der gemeinnützige **allerleih e.V.** seinen Betrieb
aufgenommen. Wer kennt es nicht: Man kauft einen teuren Gegenstand,
verwendet ihn ein, zweimal und dann liegt er Jahre lang ungenutzt im
Keller rum. Die Idee hinter allerleih ist, dass solche Gegenstände nur
einmal vom Verein gekauft und den Mitgliedern ausgeliehen wird. Schaut
doch mal rein:

<a href="#" onclick="flipdot.loadIframe(event, 'iframe_youtube_allerleih', 'https://www.youtube-nocookie.com/embed/B0zgpZm8Lj4')"><img src="/media/allerleih-youtube.png" width="675" height="315" /></a>
<iframe id="iframe_youtube_allerleih" class="hidden" allow="autoplay; gyroscope; picture-in-picture" allowfullscreen=""></iframe>

Ihr findet allerleih in der Stadtbibliothek Kassel. Weitere Infos gibt
es auf der Webseite <https://www.dein-allerleih.de/>

Wir freuen uns sehr über die Initiative und wünschen viel Erfolg!
