+++
title = "Schönes Geschenk zu Weihnachten!"
date = 2013-01-01T10:59:50Z
author = "typ_o"
path = "/blog/2013/01/01/schones-geschenk-zu-weihnachten"
aliases = ["/blog/archives/174-Schoenes-Geschenk-zu-Weihnachten!.html"]
+++
![](/media/Weihnachtsfrau1.jpeg)  
![](/media/Weihnachtsfrau2.jpeg)  
![](/media/Weihnachtsfrau3.jpeg)  
![](/media/Weihnachtsfrau4.jpeg)

Und noch eine Menge anderes Material, Prüfschnüre, Kabelrechen,
Bauteile. Ganz herzlichen Dank an die Spenderin!
