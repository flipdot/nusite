+++
title = "Gründungstreffen"
date = 2010-01-08T05:21:48Z
author = "typ_o"
path = "/blog/2010/01/08/grundungstreffen"
aliases = ["/blog/archives/61-Gruendungstreffen.html"]
+++
Am kommenden Dienstag, dem 12. Januar 2010, gründen wir im
[Kunsttempel](https://flipdot.org/blog/archives/47-Ab-jetzt-immer-Dienstags.html)
flipdot als gemeinnützigen Verein. Die Satzung ist vom Amtsgericht und
dem Finanzamt schon angesehen und abgenickt worden. Nach der Gründung
können wir ausserdem eine Beitragsordnung beschließen und damit
beginnen, etwas Geld für die ersten Ausgaben (worunter hoffentlich bald
die erste Miete für die eigenen Räume sein wird) zusammenzulegen.  
Der Verein wird als juristisches Vehikel dienen und etwas
Rechtssicherheit liefern, die inneren Strukturen von flipdot soll er
nicht abbilden.
