+++
title = "Gehäuse? Hab ich schon!"
date = 2014-06-03T22:25:08Z
author = "typ_o"
path = "/blog/2014/06/03/gehause-hab-ich-schon"
aliases = ["/blog/archives/245-Gehaeuse-Hab-ich-schon!.html"]
+++
Flipdot member \[dino\]s schicke Lösung für ein Gehäuse für das AVR EVA
Board: Bauteilkistchen!

[![](/media/avr1.serendipityThumb.jpg)](/media/avr1.jpg)

[![](/media/avr2.serendipityThumb.jpg)](/media/avr2.jpg)
