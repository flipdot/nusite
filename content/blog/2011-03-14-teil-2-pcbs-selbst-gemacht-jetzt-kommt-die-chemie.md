+++
title = "Teil 2: PCBs selbst gemacht, jetzt kommt die Chemie"
date = 2011-03-14T20:13:34Z
author = "typ_o"
path = "/blog/2011/03/14/teil-2-pcbs-selbst-gemacht-jetzt-kommt-die-chemie"
aliases = ["/blog/archives/134-Teil-2-PCBs-selbst-gemacht,-jetzt-kommt-die-Chemie.html"]
+++
Am Dienstag, dem 15.03.2011 um 20:00 h veranstalten wir im flipdot -
hackerspace kassel den zweiten Teil eines Einführungskurses in die
Leiterplattenherstellung. Diese oft grünen Platinen, die in jedem
elektronischen Gerät die Bauteile tragen, kann man mit ein wenig Aufwand
selbst herstellen, und so die eigenen Schaltungen stabil und klein
aufbauen.

Nachdem im ersten Teil die Grundlagen des Programms EAGLE für das
Zeichnen des Schaltplans und Layouten der Leiterbahnen auf der Platine
dargestellt wurden, geht es in dem zweiten Teil um den Weg von dem
fertigen Layout hin zur eigenen Platine.

Referent ist Wolfgang Jung. Der Eintritt ist frei.

In dem Workshop wird gezeigt, wie die nachfolgenden Schritte
durchgeführt werden: wie man die gezeichnete Platine auf Folie
aufdruckt, auf das Basismaterial belichtet, dieses entwickelt und ätzt,
und so in wenigen Schritten zur eigenen Platine kommt. Falls die Zeit
reicht, werden noch weitere Schritte (chemisches Verzinnen und Lötstopp)
gezeigt.

Flipdot hackerspace kassel, ein freies, unabhängiges Projekt, das eine
gemeinsam genutzte Werkstatt zum Lernen und Bausteln zur Verfügung
stellt, ist ein als gemeinnütziger Verein organisiert und lebt von den
finanziellen und tatkräftigen Beiträgen seiner Mitglieder. Jeden
Dienstag um 19:00 h ist offenes Treffen.
