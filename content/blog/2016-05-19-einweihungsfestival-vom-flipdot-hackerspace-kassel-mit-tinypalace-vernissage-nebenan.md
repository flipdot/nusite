+++
title = "Einweihungsfestival vom flipdot hackerspace kassel mit tinypalace Vernissage nebenan!"
date = 2016-05-19T06:40:03Z
author = "typ_o"
path = "/blog/2016/05/19/einweihungsfestival-vom-flipdot-hackerspace-kassel-mit-tinypalace-vernissage-nebenan"
aliases = ["/blog/archives/344-Einweihungsfestival-vom-flipdot-hackerspace-kassel-mit-tinypalace-Vernissage-nebenan!.html"]
+++
Ab heute 19:00 bis zum 22.5. am [neuen Standort im  
Hauptbahnhof Kassel](/kontakt/)
(Nachrichtenmeisterei neben Interim).

Kommt vorbei und scannt euch einen Tschunk auf der geilsten Party der
Stadt! Tanzt zur Austellung der schönsten futurologischen
Errungenschaften aus dem Blinkenlights-Experimentalfeld. Bringt eure
Kinder mit am Nachmittag und lerne haxxen: "Der Computer ist eine
subversive Maschine: Ein Freund des Volkes und  
ein Feind der Tyrannen."

lg Euer  
drei-cpeo
