+++
title = "flipdot trifft Medientheoretiker und macht Praxis"
date = 2009-11-17T11:37:05Z
author = "typ_o"
path = "/blog/2009/11/17/flipdot-trifft-medientheoretiker-und-macht-praxis"
aliases = ["/blog/archives/53-flipdot-trifft-Medientheoretiker-und-macht-Praxis.html"]
+++
Ein Vorgeschmack, was Hackerspace in Kassel sein kann: flipdot-Members
zu Gast auf der
[interfiction](https://www.interfiction.org/abstracts-cv/helmut-fligge/).

Viele "Aha"-Augenblicke und Spaß am Gerät.

[![](/media/VidTerm.serendipityThumb.GIF)](/media/VidTerm.GIF)EIDT:
Der im Film kurzzeitig sichtbare Röhrnmonitor mit dem Text
"interfiction" drauf wird getrieben von einem Mega8 mit ein [wenig
Code](/media/Vidterm64.zip) drauf, der das
PAL-Bild erzeugt. ([Q](https://www.mikrocontroller.net/topic/53140))

EDIT2: Die Musik ist übrigens von
[disrupt](https://www.jahtari.org/artists/disrupt.htm) vom Label
[jahtari](https://www.jahtari.org/), "[Blast You To
Bits](https://starfrosch.ch/2006/09/27/blast_you_bits_asteroid_dub_force)".
