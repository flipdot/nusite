+++
title = "Smartphone USB-C Hack"
date = 2020-10-23T21:08:01Z
author = "Baustel"
path = "/blog/2020/10/23/smartphone-usb-c-hack"
aliases = ["/blog/archives/447-Smartphone-USB-C-Hack.html"]
+++
![](/media/usb-c-01.serendipityThumb.jpg)

Nachdem die Micro-USB Buchse vom Smartphone kaputtgegangen ist, musste
mal wieder eine Reparatur her. Aber warum nicht gleich ein Upgrade? Also
ran an den Dremel, den Lötkolben geschwungen und in schlappen fünf
Stunden das Smartphone auf USB-C upgraden! Nachhaltigkeit pur :)

<!-- more -->

![](/media/usb-c-03.serendipityThumb.jpg)
![](/media/usb-c-04.serendipityThumb.jpg)
![](/media/usb-c-05.serendipityThumb.jpg)
![](/media/usb-c-06.serendipityThumb.jpg)
![](/media/usb-c-07.serendipityThumb.jpg)
![](/media/usb-c-08.serendipityThumb.jpg)

Material: Smartphone (LG G4; h815; 2015); USB-C Buchse,
Wiederstandsbeine, Litze, 0402 Wiederstände.
