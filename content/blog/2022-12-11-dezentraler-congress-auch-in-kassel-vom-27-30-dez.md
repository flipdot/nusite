+++
title = "Dezentraler Congress auch in Kassel, vom 27. - 30. Dez."
date = 2022-12-11T14:15:00Z
author = "elouin"
path = "/blog/2022/12/11/dezentraler-congress-auch-in-kassel-vom-27-30-dez"
aliases = ["/dC3"]
+++

Dieses Jahr wird leider kein [Congress vom Chaos Computer Club ausgerichtet](https://www.ccc.de/de/updates/2022/no-congress-2022). Es wurden aber kleinere dezentrale Events als Alternative vorgeschlagen. Auch bei uns im Hackerspace wird es ein kleines aber feines zusammenkommen geben.

An den Tagen vom 27.12. bis 30.12.22 werden wir bei uns ausgewaehlte Vortraege aus anderen Spaces per Stream anschauen, an unseren Projekten rumhacken und vielleicht auch was kochen. Am 29.12. wird es ein paar Workshops vor Ort geben und anschliessend eine kleine Party.

Schaut gerne vorbei! Eintritt frei.