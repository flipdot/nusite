+++
title = "Tag des offenen Hackerspaces 2023"
date = 2023-03-16T17:30:00Z
author = "elouin"
path = "/blog/2023/03/16/tag-des-offenen-hackerspaces-2023"
aliases = ["/tdoh23"]
+++

![Halb offene Spacetür](/media/2023-03-15-tag-des-offenen-hackerspaces-2023.jpg)

Am 25. März findet dieses Jahr der [Tag des offenen Hackerspaces](https://www.ccc.de/de/updates/2023/intopenhackerspaces) statt, an welchem Hackerspaces auf der ganzen Welt teilnehmen. Auch wir werden unsere Pforten ab 14 Uhr für euch öffnen.

Falls ihr eh vorhattet mal vorbeizuschauen und einen Einblick in unsere Räumlichkeiten und unser Treiben zu erhalten, selber Projekte habt oder auch einfach nur Lust auf ne Brause in netter Gesellschaft, dann kommt gerne vorbei.
