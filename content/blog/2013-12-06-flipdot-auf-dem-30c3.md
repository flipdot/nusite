+++
title = "Flipdot auf dem 30C3"
date = 2013-12-06T13:15:33Z
author = "typ_o"
path = "/blog/2013/12/06/flipdot-auf-dem-30c3"
aliases = ["/blog/archives/223-Flipdot-auf-dem-30C3.html"]
+++
Wir haben einen Tisch im [assembly -
Bereich](https://events.ccc.de/congress/2013/wiki/Assembly:Flipdot_hackerspace_Kassel)
angemeldet, leider wg. Verpeilung etwas zu spät - hoffentlich klappt's
noch!
