+++
title = "The Fantastic Mr Feynman"
date = 2013-11-30T07:14:23Z
author = "typ_o"
path = "/blog/2013/11/30/the-fantastic-mr-feynman"
aliases = ["/blog/archives/220-The-Fantastic-Mr-Feynman.html"]
+++
BBC-Doku über den von mir sehr verehrten Richard Feynman. Ein sehr
kluger Mensch.
