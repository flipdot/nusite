+++
title = "Ausweichquartier am 9.2.2010"
date = 2010-02-02T09:55:50Z
author = "typ_o"
path = "/blog/2010/02/02/ausweichquartier-am-9-2-2010"
aliases = ["/blog/archives/68-Ausweichquartier-am-9.2.2010.html"]
+++
Heute kann unser Treffen wegen einer raumgreifenden Ausstellung nicht im
Kunsttempel stattfinden, deswegen Ausweichquartier in der Pizzeria Da
Toni, www.pizza-datoni.de, Friedrich-Ebert-Straße 26, 34117 Mitte,
Kassel 0561 12001. Wir sind ab **19:00 Uhr** im Obergeschoß!  
Topic u.A: Planung des gemeinsamen Hack und Baustel-Wochenendes.
