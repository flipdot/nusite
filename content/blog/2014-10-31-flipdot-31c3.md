+++
title = "flipdot @ 31C3"
date = 2014-10-31T06:23:24Z
author = "typ_o"
path = "/blog/2014/10/31/flipdot-31c3"
aliases = ["/blog/archives/258-flipdot-31C3.html"]
+++
[![](/media/assembly.serendipityThumb.jpg)](/media/assembly.jpg)

Yeah, wir haben Plätze in der Assembly Hall agemeldet - werden also auch
dieses Jahr auf dem
[Kongress](https://events.ccc.de/congress/2014/wiki/Main_Page) unter
unserem Flipdot-Banner zu finden sein! Und natürlich /noch/ mehr Gerät
mit haben!
