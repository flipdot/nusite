+++
title = "2018-04-20 Übertragung des Big Brother Awards im flipdot"
date = 2018-04-13T16:39:21Z
author = "typ_o"
path = "/blog/2018/04/13/2018-04-20-ubertragung-des-big-brother-awards-im-flipdot"
aliases = ["/blog/archives/404-2018-04-20-UEbertragung-des-Big-Brother-Awards-im-flipdot.html"]
+++
[![](/media/BBA2018.serendipityThumb.jpg)](/media/BBA2018.jpg)

18:00 Uhr, im flipdot ([Anfahrt](/kontakt/))
übertragen wir die Verleihung des [Big Brother
Awards](https://bigbrotherawards.de/). (Keine Kosten)  

> Die Big Brother Awards (BBA) sind Negativpreise, die jährlich in
> mehreren Ländern an Behörden, Unternehmen, Organisationen und Personen
> vergeben werden. Die Preise werden, so die Stifter, an die verliehen,
> die in besonderer Weise und nachhaltig die Privatsphäre von Personen
> beeinträchtigen oder Dritten persönliche Daten zugänglich gemacht
> haben oder
> machen.([Q](https://de.wikipedia.org/wiki/Big_Brother_Awards))

Falls darüber hinaus noch was stattfindet (Grillen oder so) steht das
dann rechtzeitig hier.  
Wer das Sofa nicht verlassen will, kann den [Stream selber
anmachen](https://bigbrotherawards.de/stream).
