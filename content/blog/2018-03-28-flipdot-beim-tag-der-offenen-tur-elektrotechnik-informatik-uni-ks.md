+++
title = "flipdot beim Tag der offenen Tür Elektrotechnik / Informatik Uni KS"
date = 2018-03-28T09:27:15Z
author = "typ_o"
path = "/blog/2018/03/28/flipdot-beim-tag-der-offenen-tur-elektrotechnik-informatik-uni-ks"
aliases = ["/blog/archives/400-flipdot-beim-Tag-der-offenen-Tuer-Elektrotechnik-Informatik-Uni-KS.html"]
+++
Wir sind wieder mit einem Stand auf dem Tag der offenen Tür des
Fachbereichs Elektrotechnik / Informatik der Uni Kassel vertreten.

Wann: 12.04.2018 ab 14:30 Uhr  
Wo: Wilhelmshöher Allee 71-73, 34121 Kassel

kommt uns besuchen, und schaut euch an, was wir so machen!
