+++
title = "Schlösser von Hackern für Hacker"
date = 2009-10-05T05:38:00Z
author = "typ_o"
path = "/blog/2009/10/05/schlosser-von-hackern-fur-hacker"
aliases = ["/blog/archives/20-Schloesser-von-Hackern-fuer-Hacker.html"]
+++
Einen Schlüssel? Blah! RFID-Chips? Gnah! Richtige Hacker öffnen ihre
Räume [via login per ssh mit
rsa-key](https://wiki.muc.ccc.de/luftschleuse), username 'open' -\>
öffnen, username 'close' -\> schliessen. So geht das!
