+++
title = "Flipdot zieht um! Mehr, besser, Glitzer, und alles!"
date = 2016-04-04T16:06:00Z
author = "typ_o"
path = "/blog/2016/04/04/flipdot-zieht-um-mehr-besser-glitzer-und-alles"
aliases = ["/blog/archives/340-Flipdot-zieht-um!-Mehr,-besser,-Glitzer,-und-alles!.html"]
+++
Wir verlassen die 25 m² in der Chassalla Fabrik in der Sickingenstraße,
und haben jetzt neue Räume im "Kulturbahnhof", Franz-Ulrich-Straße 18.

[![](/media/anfahrt.serendipityThumb.png)](/media/anfahrt.png)

Die letzte Meile: Vom Bahnhofsvorplatz zum Space.

[Details zu den neuen Räumen](https://flipdot.org/wiki/Neuland).

Ab dem 12. April 2016 sind wir dort auch für Besucher zu finden.
Einzugsparty kommt!
