+++
title = "RC3 - Remote Chaos Communication Congress - Arduino Workshop"
date = 2020-12-13T11:00:29Z
author = "Baustel"
path = "/blog/2020/12/13/rc3-remote-chaos-communication-congress-arduino-workshop"
aliases = ["/blog/archives/449-RC3-Remote-Chaos-Communication-Congress-Arduino-Workshop.html"]
+++
Der jährliche
[Kongress](https://de.wikipedia.org/wiki/Chaos_Communication_Congress)
des [CCC](https://www.ccc.de/) findet dieses Mal [rein
online](https://events.ccc.de/2020/09/04/rc3-remote-chaos-experience/)
statt. [flipdot](https://flipdot.org) bietet u.A. einen Workshop für
absolute Laien an: Ein Einstieg in die Welt der Programmierung von
Mikrocontrollern des
[Arduino-Systems](https://de.wikipedia.org/wiki/Arduino_(Plattform)).

[![](/media/arduino-rc3-2.serendipityThumb.JPG)](/media/arduino-rc3-2.JPG)

<!-- more -->

- Keine Elektronik- oder Programmierkenntnisse nötig
- Du brauchst einen Rechner mit USB Schnittstelle
- Workshop-Material wird leihweise gegen 100 € Pfand bereit gestellt
(Eigene Arduinos natürlich auch möglich), der Workshop selbst ist
kostenfrei.
- max. 15 Teilnehmer:innen mit unserem Material, dazu TN mit eigenen
Arduinos
- Anmeldung: com \[kringeldings\] flipdot \[punkt\] org, Betreff bitte
arduino-rc3.
- Termine vorauss. 27.,28., und 29.12.2020 10:30-13:00
- Materialabholung am [flipdot HbF
Kassel](/kontakt/), vorauss. 26.12.2020, 18:00
Uhr.

Im Workshop installieren wir die Arduino-Entwicklungsumgebung, lassen
den Arduino auf Sensor-Inputs reagieren, blinken, Text anzeigen und
Dinge bewegen. Gerne könnt ihr auch mit Ideen zu eigenen Projekten
kommen! Wir freuen uns schon auf viel Spaß am Gerät!

Wenn ihr euch das Material selbst beschaffen wollt, hier ist ein
[Reichelt-Warenkorb](https://www.reichelt.de/my/1787734) dazu. Relativ
teuer, weil bequemes, konfektioniertes System. Es tut auch ein
einfach-Arduino, ein paar Drähte LED usw.
