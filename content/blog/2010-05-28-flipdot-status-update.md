+++
title = "flipdot status update"
date = 2010-05-28T16:30:40Z
author = "typ_o"
path = "/blog/2010/05/28/flipdot-status-update"
aliases = ["/blog/archives/89-flipdot-status-update.html"]
+++
**Wir haben (jetzt wirklich) Räume!** Die Mietverträge liegen hier vor
mir auf dem Tisch. Wir werden ab 1. Juni in dem Hausprojekt
[Chasalla](https://fabrik-chasalla.de/) in der Sickingenstraße unseren
Ort haben, einem Haus in dem es schon 30 Gruppen und Initiativen gibt.
Heute haben wir eine große LKW-Ladung Möbel und Ausrüstung in die Räume
gefahren, hier ein paar Blicke:  
[![](/media/IMAG0216.serendipityThumb.jpg)](/media/IMAG0216.jpg)[![](/media/IMAG0209.serendipityThumb.jpg)](/media/IMAG0209.jpg)[![](/media/IMAG0210.serendipityThumb.jpg)](/media/IMAG0210.jpg)[![](/media/IMAG0211.serendipityThumb.jpg)](/media/IMAG0211.jpg)[![](/media/IMAG0212.serendipityThumb.jpg)](/media/IMAG0212.jpg)[![](/media/IMAG0213.serendipityThumb.jpg)](/media/IMAG0213.jpg)  
Morgen werden wir um 19:00 ein letztes Treffen im Kunsttempel haben -
die Mitgliederversammlung, die formal noch beschließt, dass wir die
Räume mieten wollen.
