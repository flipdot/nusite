+++
title = "Ab jetzt immer Dienstags"
date = 2009-10-26T11:12:00Z
author = "typ_o"
path = "/blog/2009/10/26/ab-jetzt-immer-dienstags"
aliases = ["/blog/archives/47-Ab-jetzt-immer-Dienstags.html"]
+++
flipdot - Treffen im Kunsttempel Kassel. **19:00h**. Interessiert?
Einfach dazukommen.

[kunsttempel.net](https://www.kunsttempel.net), und [Google
Maps](https://maps.google.com/maps?f=q&source=s_q&hl=de&geocode=&q=Friedrich-Ebert-Str.+177,+kassel&sll=37.0625,-95.677068&sspn=23.761683,55.810547&ie=UTF8&hq=&hnear=Friedrich-Ebert-Stra%C3%9Fe+177,+West+34119+Kassel,+Hessen,+Deutschland&ll=51.316089,9.460862&spn=0.00057,0.002725&t=h&z=19),
Anreisekoordinaten für Geocacher: 51.316065,9.460679 ;)  
Versuch einer Tages"ordnung" <a href="#"><del>hier</del></a>, bitte editieren.
