+++
title = "Neuer Aggregator für Hackerspaces - Blogs: Spaceblogs"
date = 2013-01-02T09:05:31Z
author = "typ_o"
path = "/blog/2013/01/02/neuer-aggregator-fur-hackerspaces-blogs-spaceblogs"
aliases = ["/blog/archives/175-Neuer-Aggregator-fuer-Hackerspaces-Blogs-Spaceblogs.html"]
+++
[hackerspaces.soup.io](https://hackerspaces.soup.io/) scheint inaktiv zu
sein, Ersatz gibts unter [spaceblogs.org](https://spaceblogs.org/). Hab
unseren Feed eingetragen.
