+++
title = "HackWat! 1. bis 3. Juni 2018 im flipdot hackerspace kassel"
date = 2018-04-08T07:36:57Z
author = "typ_o"
path = "/blog/2018/04/08/hackwat-1-bis-3-juni-2018-im-flipdot-hackerspace-kassel"
aliases = ["/blog/archives/403-HackWat!-1.-bis-3.-Juni-2018-im-flipdot-hackerspace-kassel.html"]
+++
[![](/media/HackWat_vorn_kl.serendipityThumb.png)](https://flipdot.org/wiki/HackWat)

[![](/media/HackWat_hinten_kl.serendipityThumb.png)](https://flipdot.org/wiki/HackWat)

Drei Tage lang ist der Hackerspace geöffnet.

- Workshops und Vorträge (Elektronik, Praxis-Schwerpunkt
  Verschlüsselung von Dateien und Mail, und vieles mehr)
- Werkstätten kennen lernen (Elektronik und Mechanik, Infos zur CNC
  Fräse und 3D Druck)
- Projekte ansehen (Automatisches Getränke- Bestellsystem, Internet of Dings,
  Blinkenlights, Member-Schließsysteme und viele kleinere Projekte)
- Infos zu Software-Projekten und Space Infrastruktur
- Mitglieder treffen und kennen lernen
- Hackerkompatible Verpflegung

Die stabile Version des Fahrplans steht frühestens ab 20. Mai an [dieser
Stelle](https://flipdot.org/wiki/HackWat).
