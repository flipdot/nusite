+++
title = "Veranstaltungshinweis: I²oT Hackathon der Uni Kassel"
date = 2018-11-01T17:20:00Z
author = "Baustel"
path = "/blog/2018/11/01/veranstaltungshinweis-i2ot-hackathon-der-uni-kassel"
aliases = ["/blog/archives/420-Veranstaltungshinweis-IoT-Hackathon-der-Uni-Kassel.html"]
+++
Der
[I²oTHack](https://www.ies.uni-kassel.de/p/i%C2%B2othack/ "I²oTHack")
ist ein Hackathon vom 16.11 – 18.11., der vom Fachgebiet „Intelligent
Embedded Systems“ der Universität Kassel organisiert wird. An dem
Wochenende sollen in der Mensa 73 kleinere Projekte aus dem Bereich
Intelligent Internet of Things umgesetzt werden. Zur Verfügung stehen
Kaffee, Mate, Essen, Lötbauteile, Raspberry Pis, 3D-Brillen,
Sensorstühle, Touchbildschirme, Kinnects, Kameras, und noch mehr
Sensorik.

Einige Mitglieder vom flipdot werden ebenfalls teilnehmen und sich dort
austoben.

Mehr Informationen und Anmeldung unter
[https://www.ies.uni-kassel.de/p/i²othack/](https://www.ies.uni-kassel.de/p/i²othack/ "https://www.ies.uni-kassel.de/p/i²othack/")
