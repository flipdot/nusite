+++
title = "Hackerspace Happenings: Hackerspace Symposium in Berlin"
date = 2012-01-19T05:07:34Z
author = "typ_o"
path = "/blog/2012/01/19/hackerspace-happenings-hackerspace-symposium-in-berlin"
aliases = ["/blog/archives/158-Hackerspace-Happenings-Hackerspace-Symposium-in-Berlin.html"]
+++
The Hackerspaces.org blog announced a symposium at the c-base
hackerspace called “[Hackerspaces: The story so far and the future
ahead](https://blog.hackerspaces.org/2012/01/13/upcoming-symposium-in-berlin-germany-hackerspaces-the-story-so-far-and-the-future-ahead/)”
to be held on Friday, February 3rd.
