+++
title = "Review PPN"
date = 2016-12-02T10:35:30Z
author = "typ_o"
path = "/blog/2016/12/02/review-ppn"
aliases = ["/blog/archives/361-Review-PPN.html"]
+++
Pizzaprogrammiernacht \\o/

- Raum 4 hat gut funktioniert, fast durchgehend belegt
- Schöne Netzwerkspielzeuge (Drucker, Flipdots, Blinkenlights)
- Pizza war gut
- Viel Besuch von Ausserhalb
- Lasertag Kooperation war gut, günstier Preis, gut dass es Bewegung gab.
- Nette Leute, gute Atmosphäre

Infrastructure Review

- Pizzaproduktions - Evaluation 147 Stück, 28 kg Mehl =\> 43,5 kg Teig
- Dickstrom-Verbrauch (annähernd) 23 kWh
- Besucherzahlen vemutlich zwischen 40 bis 50
