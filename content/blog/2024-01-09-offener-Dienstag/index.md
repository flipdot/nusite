+++
title = "open Hackspace 2024-01-09"
date = 2024-01-09T11:15:00Z
author = "and…"
path = "/blog/2024/01/09/open-hackspace"
draft = false
+++

## Telefone
gestern, an unserem offen Dienstag hat die Bude gebrummt!
Es wurden DTMF Telefone gehackt also welche noch mit Tonwahlverfahren arbeiten. Was auf dem einen Bild zu sehen ist, ist ein VoIP (Voice over IP) Interface bei dem die Telefone angeschlossen werden können um über das Internet ins echte Telefonnetz zu telefonieren. Aber sie können sich auch, ohne übers Internet verbunden zu sein, gegenseitig anrufen. Man kann dann mit einer zweitelligen Rufnummer die Telefone untereinander anrufen. Ziel ist es auch das man auch noch Wahlscheiben Geräte (Impulswahl) anschließen kann...

## Türöffner
Aus dem Chaos wurde auch zielsicher unsere CNC-Fräse gefischt, kurzerhand verkabelt und begonnen damit ein neues Türschliesssystem zu fräsen. Später bekommen wir darüber 24/7 Zutritt, man kann dann jeder Member von flipdot (Credentials müssen natürlich hinterlegt sein) entweder eine Rufnummer anrufen oder über ein Webinterface einen Button klicken (SSH bleibt selbstverständlich weiterhin erhalten... bitte weiteratmen)... zack passiert Magie und die Tür geht wie von Geisterhand auf.


### Wer mehr darüber wissen möchte kommt einfach nächsten Dienstag in den freundlichen Hackspace um die Ecke und lässt sich das erklären... oder macht einfach mit

Hier wieder ein paar Bilder... geht ja nicht ohne

## Tür gebaustel:
![Türschliesssystem, how it begins](IMG_2029.jpg)
![Bauteile für das Schliesssystem zeichnen](IMG_2031.jpg)
![Fräsen](IMG_2030.jpg)
![fertiges Teil an die Tür bausteln](IMG_2032.jpg)
## wildes Elektrowerkstatt hacken:
![wildes Elektrowerkstatt hacken](IMG_2034.jpg)
![Das VoIP-Interface](IMG_2035.jpg)
