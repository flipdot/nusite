+++
title = "Beheiztes Mauspad"
date = 2018-08-15T16:00:40Z
author = "typ_o"
path = "/blog/2018/08/15/beheiztes-mauspad"
aliases = ["/blog/archives/412-Beheiztes-Mauspad.html"]
+++
Jaaaaa, *heute* denkt keiner dran, aber der Winter kommt, und kalte
Zockerhände! Deswegen eine Heizfolie auf einer Aluplatte, und oben
drauf etwas Furnier. Ein Sensor DS18B20, ein AVR und ein dicker MOSFET
zum schalten.

[![](/media/20180815_153946.serendipityThumb.jpg)](/media/20180815_153946.jpg)

[![](/media/maus2.serendipityThumb.jpg)](/media/maus2.jpg)

[![](/media/maus1.serendipityThumb.jpg)](/media/maus1.jpg)
