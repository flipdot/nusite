+++
title = "OKM Soundworkshop"
date = 2025-02-15T11:29:35.722Z
author = "and…"
path = "/blog/2025/02/15/OKM-Soundworkshop"
draft = false
+++
## Tauchen wir ein in die Klangwelten des Unsichtbaren!

In unserem Workshop experimentieren wir mit innovativen OKM (Original Kopf Mikrofone), die wir direkt im Ohr tragen – für realitätsnahe, raumklangreiche Aufnahmen via mobiler Apps.

![OKM Soundworkshop](/media/okm.png "OKM Soundworkshop")

Wir lernen, wie spezielle Sensoren elektromagnetische Felder in faszinierende Klänge umwandeln und
gestalten gemeinsam unser eigenes Mini-Hörspiel. So
schaffen wir Klangkunst, welche die verborgenen, unsichtbaren Wellen kreativ zum Leben erweckt!

Die von uns aufgenommenen Klänge werden mit
Open‑Source‑Tools weiterverarbeitet und für eine Veröffentlichung vorbereitet.

Der Workshop ist kostenlos.

Sonntag 30.03.2025 • 14:00 - 18:00 Uhr

Wegen der begrenzten Ressourcen ist daher eine verbindliche Anmeldung nötig: typ_o@flipdot.org

[Hier dann noch der Flyer zum Download](/media/okm_flyer_digital.pdf)
