+++
title = "RECHARGE flipdot Party"
date = 2023-06-03T12:30:00Z
author = "elouin"
path = "/blog/2023/06/03/recharge-flipdot-party"
+++

![Flipdot RECHARGE party Logo](/media/2023-06-03-recharge-flipdot-party.gif)

Party-Time im flipdot hackspace Kassel:

„RECHARGE flipdot“ 23. Juni 2023, Eintritt frei

Wir haben da was für euch! Am 23. Juni öffnen wir ab 14 Uhr unsere Türen und zeigen euch die neuesten Projekte aus den Bereichen Hardware, Software, Elektronik und Mechanik. Ihr werdet staunen und euch inspirieren lassen. Seht unsere Werkstätten und Hackcenter, Blinkenlights und Projektionen, unser Lager mit Hack-mich-Zeug, und Kabel! So viel Kabel!

In einer offenen Diskussionsrunde sprechen wir zusammen über künstliche Intelligenz. Lasst uns unsere Gedanken austauschen, Fragen stellen und voneinander lernen. Wir freuen uns auf euren Input.

Es gibt Kurz-Talks zu hackrelevanten Themen, und jede Menge Gelegenheit zum Gespräch mit Membern: Ask Me Anything!

Hungrig? Kein Problem! Wir haben einen richtig großen Gastro-Pizzaofen in unserer Küche und werden frische Pizzen für euch backen. Dazu gibts natürlich Mate und Tschunk.

Und na klar gibts Mucke! Wir drehen die Anlage auf und lassen den Bass pumpen. Tanzt, chillt oder klappt unbeeindruckt einfach euren Rechner auf: Musik, gute Laune und gute Leute!

Bringt eure Mit-Nerds und Freunde mit und lasst uns gemeinsam einen genialen Tag voller Technik, interessanter Gespräche und netter Stimmung erleben.

Also, markiert euch den 23. Juni im Kalender und seid dabei! Wir freuen uns auf Euch!

flipdot <br>
Schillerstraße 25<br>
34117 Kassel<br>
<br>
<iframe src="https://player.vimeo.com/video/214990700?h=e94e12f0f0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

