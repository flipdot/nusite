+++
title = "Thermoelektrischer Generator"
date = 2013-12-19T16:25:40Z
author = "typ_o"
path = "/blog/2013/12/19/thermoelektrischer-generator"
aliases = ["/blog/archives/225-Thermoelektrischer-Generator.html"]
+++
Wegen
[dieses](https://clockworker.de/cw/2013/12/03/der-thermoelektrische-aetherwellen-empfaenger/)
Blogartikels hab ich in Reisefotos gekramt und den [thermoelektrischen
Generator](https://de.wikipedia.org/wiki/Thermoelement) herausgesucht,
den ich im [Museum für
Kommunikationsgeschichte](https://www.teo.lt/node/1066) in Kaunas
fotografiert habe. (In
[diesem](https://www.teo.lt/gallery/flash/Telekomun.swf) Flashpanorama
findet man das Gerät sogar in einer Ecke).

[![](/media/seebeck_kaunas_2.serendipityThumb.jpg)](/media/seebeck_kaunas_2.jpg)

[![](/media/seebeck_kaunas_1.serendipityThumb.jpg)](/media/seebeck_kaunas_1.jpg)

Augenscheinlich treibt der Generator ein Röhrenradio, allerdings nicht
direkt, sondern vermutlich über einen Akku als Zwischenspeicher
(Zeichnung rechts unten). (Das selbe in neu gibts wohl
[hier](https://thermalforce.de/))
