+++
title = "Letzte Nacht-Hacksession:"
date = 2017-02-27T11:08:59Z
author = "typ_o"
path = "/blog/2017/02/27/letzte-nacht-hacksession"
aliases = ["/blog/archives/368-Letzte-Nacht-Hacksession.html"]
+++
Hostapd  
{  
    - Simple bridge setup  
    - 16.04  
    - /etc/network/interfaces \<3  
    - iw dev set 4addr on  
}  
Networking  
{  
    - 3/4 debugging  
}  
Openwrt  
{  
    - LuCI rulez  
    - client mode  
    - upstream  
    - wan/lan nat  
    - dhcp/dns  
}
