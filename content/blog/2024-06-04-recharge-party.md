+++
title = "RECHARGE flipdot Party"
date = 2024-06-04T12:30:00.000Z
author = "elouin"
path = "/blog/2024/06/04/recharge-party"
aliases = ["/recharge", "/RECHARGE"]
draft = false
+++
![Flipdot RECHARGE party Logo](/media/2023-06-03-recharge-flipdot-party.gif)

Einladung zur Party im flipdot Hackspace Kassel:

„RECHARGE flipdot“ am 22. Juni 2024, Eintritt frei

Wir laden euch herzlich ein, am 22. Juni ab 16 Uhr unsere neuesten Projekte aus den Bereichen Hardware, Software, Elektronik und Mechanik zu entdecken. Lasst euch inspirieren und werft einen Blick in unsere Werkstätten und Hackcenter. Es gibt viel zu sehen: von Blinkenlights und Projektionen bis hin zu unserem umfangreichen Lager voller spannender Materialien und Kabel.

Zusätzlich gibt es Kurz-Talks zu verschiedenen Hack-Themen und reichlich Gelegenheit, sich mit unseren Mitgliedern zu unterhalten: fragt uns alles, was ihr wissen wollt!

Für euer leibliches Wohl ist gesorgt: in unserer Küche bereiten wir frische Pizzen im großen Gastro-Pizzaofen zu. Dazu gibt es Mate, Tschunk und andere Getränke.

Natürlich darf auch die Musik nicht fehlen! Wir sorgen für die passende Stimmung mit guter Musik zum Tanzen, Chillen oder einfach nur zum Genießen.

Bringt eure Freunde und Mit-Nerds mit und erlebt mit uns einen tollen Tag voller Technik, spannender Gespräche und guter Laune.

Also, markiert euch den 22. Juni im Kalender und seid dabei! Wir freuen uns auf euch!\
\
PS.:\
Unsere neue Adresse ist Schillerstraße 25 34117 Kassel. Wie ihr am besten zu uns kommt steht auf unserer [Kontakt](https://flipdot.org/kontakt) Seite.
