+++
title = "flipdot Status update"
date = 2009-11-18T05:05:05Z
author = "typ_o"
path = "/blog/2009/11/18/flipdot-status-update"
aliases = ["/blog/archives/54-flipdot-Status-update.html"]
+++
Bis auf die Vereinsziele (wirkt auf Gemeinnützigkeit) steht die Satzung
jetzt. Dennis und Helmut gehen am Freitag zum Finanzamt, um im Gespräch
zu klären, wie detailliert die Erläuterungen sein müssen. Sobald unser
Edit von dort das ok bekommt, gründen wir den flipdot e.V.

**EDIT** 20.11.09: Termin war gut, haben mündlich, dass die Fassung vom
19.11. ok ist, die Sachbearbeiterin hat versprochen, uns das schriftlich
zu geben, damit wir nach der Gründung nicht nochmal ändern müssen - wäre
doof.
