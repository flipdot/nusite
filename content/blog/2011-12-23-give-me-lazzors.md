+++
title = "Give me Lazzors!"
date = 2011-12-23T05:28:01Z
author = "typ_o"
path = "/blog/2011/12/23/give-me-lazzors"
aliases = ["/blog/archives/154-Give-me-Lazzors!.html"]
+++
[Laser Open Source](https://www.laoslaser.org/) (LAOS) beschäftigt sich
mit der Verbesserung billiger Chinalasercutter durch eigene Elektronik
und Software, und [Lasersaur](https://labs.nortd.com/lasersaur/) will
gleich einen kompletten Open Hardware Laser bauen!
