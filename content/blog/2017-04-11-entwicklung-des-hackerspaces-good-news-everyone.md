+++
title = "Entwicklung des Hackerspaces - Good News Everyone!"
date = 2017-04-11T07:44:17Z
author = "typ_o"
path = "/blog/2017/04/11/entwicklung-des-hackerspaces-good-news-everyone"
aliases = ["/blog/archives/372-Entwicklung-des-Hackerspaces-Good-News-Everyone!.html"]
+++
[![](/media/Bildschirmfotovom2017-04-11083417.serendipityThumb.png)](/media/Bildschirmfotovom2017-04-11083417.png)

Für Mitglieder ist der Space jeden Tag zugänglich. Am offenen Dienstag
sind immer \>\> 20 Menschen da (Mehr als in dem Graphen, da die Zählung
auf eingebuchten Rechnern beruht, und Gäste oft keine Geräte im Wlan
anmelden). Erfreulicher Weise ist der Space auch an allen anderen Tagen
sehr gleichmäßig besucht (Dienstag ist eher so Party Mode, und eine gute
Gelegenheit zum Socializing).

[![](/media/members.serendipityThumb.png)](/media/members.png)  
In den letzten fünf Quartalen hat sich die Zahl der Menschen, die den
Space durch ihren Beitrag am Leben halten, fast verdoppelt. Die Beiträge
pro Member sinken etwas, weil gerade viel Schüler und Studenten dazu
kommen - und das ist positiv! (Wir haben eine sehr individuelle
Beitragspolitik - und finden für jeden, der wenig Geld hat, eine
Lösung!).

Wir sind jetzt genau ein Jahr in den neuen Räumen im Kulturbahnhof - der
Umzug war dringend nötig und hat sich absolut gelohnt. Viel bessere
Möglichkeiten, Platz für alle Aktivitäten, eine [heftig genutzte
Küche](https://flipdot.org/blog/archives/364-Food-Pr0n.html), und nette
Nachbarn.

Seit dem Gründungstreffen am 6.10.2009 ist der Space kontinuierlich
gewachsen und hat auch einige Krisen überstanden (Wir haben gelernt,
nicht p0wned zu werden ;)
