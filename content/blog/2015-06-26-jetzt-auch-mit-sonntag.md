+++
title = "Jetzt auch mit Sonntag!"
date = 2015-06-26T16:26:01Z
author = "flipdot member"
path = "/blog/2015/06/26/jetzt-auch-mit-sonntag"
aliases = ["/blog/archives/305-Jetzt-auch-mit-Sonntag!.html"]
+++
Wir haben jeden Dienstag ab 19:00 Uhr für Interessenten geöffnet. Um  
diesen Abend etwas zu entlasten (es ist manchmal echt sehr voll)
machen  
wir jetzt auch Sonntags auf. So ab späten Nachmittag, guckt einfach  
rechts oben auf das Icon mit der Tür!
