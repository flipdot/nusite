+++
title = ".. hat ein Platinenfräsgerät."
date = 2017-07-29T06:34:39Z
author = "Baustel"
path = "/blog/2017/07/29/hat-ein-platinenfrasgerat"
aliases = ["/blog/archives/386-..-hat-ein-Platinenfraesgeraet..html"]
+++
[![](/media/cool.serendipityThumb.jpg)](/media/cool.jpg)

Im FlipDot.

Doppelseitige Platine für SMD-Bauteile und Buchsen/Stecker.

Hier:

- kleinste Bohrung 0,8 mm  
- kleinste Leiterbahnbreite 0,6 mm, bei genauer Einstellung geht es

noch feiner.

Schritte:

- Schaltplan und Layout mit "Eagle", mit Eagles CAM-Processor exportiert.
- Mit dem Programm "FlatCAM", Werte für die Tiefe, Geschwindigkeit,
  Breite eingestellt, etc.. und in CNC-Dateien sowie G-Code umgewandelt
- Mit Universal "G-Code Sender" die Fräse auf den Nullpunkt der Platine
  eingestellt und die G-Code Dateien nach und nach an die Fräse geschickt.
- Mit der Fräse die Löcher gebohrt, Isolation gefräst, Platine gewendet und
  Fräse an extra Passmarken neu zentriert, Isolation der Rückseite gefräst,
  ausschneiden lassen.

Fertig :)

Vom fertigen Eagle-Layout bis zu dieser doppelseitigen Platine, ca. 4
Std. mit Fräszeit, relativ unerfahren.


[![](/media/CoolOben.serendipityThumb.jpg)](/media/CoolOben.jpg)
Oben

[![](/media/CoolSMD.serendipityThumb.jpg)](/media/CoolSMD.jpg)
Mit SMD Bauteilen und lecker oxidierenden Fingerabdrücken, da keine Handschuhe
und "nur" Prototyp.

Wer sich eine Platine fräst, der hat ein Platinenfräsgerät.

**Im FlipDot!**

Io
