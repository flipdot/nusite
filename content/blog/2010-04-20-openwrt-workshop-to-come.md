+++
title = "OpenWRT Workshop to come"
date = 2010-04-20T08:46:27Z
author = "typ_o"
path = "/blog/2010/04/20/openwrt-workshop-to-come"
aliases = ["/blog/archives/82-OpenWRT-Workshop-to-come.html"]
+++
Ich werd heute mal einige Router mitbringen auf denen OpenWRT läuft.  
An denen können wir ein bisschen rumspielen. Wir können mal schauen
welche Router am besten geeignet sind für OpenWRT, vielleicht noch
Alternativen ansehen. Und dann können wir nächte Woche vielleicht an den
eigenen Routern rumspielen.  
[OpenWRT](https://www.openwrt.org/), die [Liste der unterstützten
Hardware](https://wiki.openwrt.org/toh/start). Ich kann den WL-500g
Premium v1 entfehlen. Den bring ich mit. Linksys WRT54GL bring ich mit,
gibt's aber nichtmehr. WRTSL54GS oder WRT54G3GV2-VF könnte das neue
"top" Model sein. Also bis morgen dann, Emzy

Das und mehr (neue potentielle Räume) heute, Dienstag 19:00 im
[Kunsttempel](https://flipdot.org/blog/archives/47-Ab-jetzt-immer-Dienstags.html).
