+++
title = "Leiterplatten geätzt & gefräst"
date = 2017-07-18T01:44:01Z
author = "typ_o"
path = "/blog/2017/07/18/leiterplatten-geatzt-gefrast"
aliases = ["/blog/archives/384-Leiterplatten-geaetzt-gefraest.html"]
+++
Film: Laserdruck auf transparentem Zeichenpapier von Brunnen, besprüht
mit Tonerverdichter. Bungard Basismaterial belichtet in umgebautem
Scanner, entwickelt mit Natriumhydroxid, geätzt mit Natriumpersulfat.

Zwei diagonal gegenüberliegende Passbohrungen in der geätzten Platine
mit 3 mm, Passfräsungen in der MDF-Opferplatte mit 2,5 mm.
Platinenbefestigung mit M3 Schrauben.

[![](/media/20170716_193339.serendipityThumb.jpg)](/media/20170716_193339.jpg)
