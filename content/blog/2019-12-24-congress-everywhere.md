+++
title = "Congress Everywhere"
date = 2019-12-24T14:52:03Z
author = "typ_o"
path = "/blog/2019/12/24/congress-everywhere"
aliases = ["/blog/archives/431-Congress-Everywhere.html"]
+++
[![](/media/98ac74ff4bab49f7d4f7ff742ec10627edd141fd.serendipityThumb.png)](/media/98ac74ff4bab49f7d4f7ff742ec10627edd141fd.png)

Während des
[CCCongresses](https://www.ccc.de/en/updates/2019/36c3-in-leipzig "cccongress")
vom 27. bis zum 30. Dezember mit ca. 17.000 Teilnehmern bieten wir für
alle, deren Comfort Zone nicht für so viele Menschen Platz bietet, oder
die aus anderen Gründen nicht nach Leipzig fahren können, wieder
Congress Everywhere an:
[Vorträge](https://fahrplan.events.ccc.de/congress/2019/Fahrplan/) im
[Congress-Stream](https://streaming.media.ccc.de/ "stream") gucken,
bausteln, Pizza Essen im kleinen Rahmen in unseren gerade neu bezogenen
Räumen ([gleicher Ort](/kontakt/ "Anfahrt"), ein
Stockwerk tiefer).

Ab dem Nachmittag bis in den Abend - guckt auf unserem Blog rechts oben,
ob der Space auf ist, oder ruft einfach an: +49 561 47395848
