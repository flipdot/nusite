+++
title = "Kaufen ist lame,"
date = 2009-10-04T06:38:46Z
author = "typ_o"
path = "/blog/2009/10/04/kaufen-ist-lame"
aliases = ["/blog/archives/17-Kaufen-ist-lame,.html"]
+++
aber an einem trüben Nachmittag mal durch diese Links zu gehen und zu
überlegen, was dabei rauskäme, wenn man zum Einen oder Anderen ein
wenig Arduino, etwas Ethernet oder ein paar Leuchtdioden geben würde,
macht Spass. Im Zweifel kann man ja immer noch die Weihnachtswunschliste
verlängern ;). Remix, resample Gadget Shops:
[getdigital](https://www.getdigital.de/),
[buglabs](https://www.buglabs.net/products),
[meninos](https://www.meninos.us/),
[styleon](https://www.styleon.de/index.html?cmd=setshoprubrik&rubrik=283),
[artlebedev](https://www.artlebedev.com/),
[mathe-shirts](https://www.mathe-shirts.de/mathemagiker/gauss/),
[boysstuff](https://www.boysstuff.co.uk/),
[thinkgeek](https://www.thinkgeek.com/),
[geekgiftshop](https://www.geekgiftshop.net/content-categories/cat-403_404/illuminated_shirts.html),
[g33kshop](https://www.g33kshop.com/),
[bytelove](https://www.bytelove.com/stuff/gadgets/cat_14.html)
[nprshop](https://shop.npr.org/catalog/Geek_Bouteek-34-1.html),
[3dsupply](https://www.3dsupply.de/shop/index.php),
[ausgefallene-ideen](https://blog.ausgefallene-ideen.com/).
