+++
title = "Do-Ism (Sachen machen)"
date = 2009-09-23T20:08:00Z
author = "typ_o"
path = "/blog/2009/09/23/do-ism-sachen-machen"
aliases = ["/blog/archives/6-Do-Ism-Sachen-machen.html"]
+++
flipdot (so lautet zumindest der beta-Name) ist nun auch bei
[Hackerspaces.org als Hackerspace im
Aufbau](https://wiki.hackerspaces.org/List_of_Hacker_Spaces) geführt.
Auf [Twitter](https://twitter.com/flipdot_kassel) gibt es schon 11 Follower,
und einige explizite Anmeldungen für die Startveranstaltung am 6.
Oktober im Fiasko.
