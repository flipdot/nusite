+++
title = "Status update"
date = 2012-01-26T05:25:34Z
author = "typ_o"
path = "/blog/2012/01/26/status-update"
aliases = ["/blog/archives/159-Status-update.html"]
+++
Aleksej und Burkhard (und Heribert?) haben die Decke gestrichen und
Fußleisten angeschliffen. Beim Bodenbelag fiel die Entscheidung für
Entfernung weil buckelig und echt fertig, dabei kam diese Interessante
Abdeckung zum Vorschein. "Sollen wir das mal aufschrauben?" - Na klar
sollen wir das mal aufschrauben!

![](/media/beide.jpg)

Burkhard "Aufputz" B. hat sich um Steckdosen und Lampenbestromung
gekümmert und Jürgens Leuchtstofflampen angebracht, der Arebitsraum
hinten benötigt jetzt nur noch Bodenbelag (schon gekauft und im Raum),
dann kann eingeräumt werden. Unter das PVC soll jetzt doch OSB, zum
Ausgleich.

![](/media/IMAG1549.jpg)

Nochmal zwei Montage- und Justagestunden von Jürgen und Helmut später
ist das Schloss jetzt im finalen \[TM\] Stadium - Neue Endschalter und
besseres Timing lösten die mechanischen deadlocks.
