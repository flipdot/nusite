+++
title = "flipdot Status update"
date = 2009-12-15T09:54:44Z
author = "typ_o"
path = "/blog/2009/12/15/flipdot-status-update"
aliases = ["/blog/archives/59-flipdot-Status-update.html"]
+++
Die Satzung ist jetzt pre-release. Wir haben die Änderungswünsche des
Amtsgerichts eingearbeitet und die Satzung nochmal vorgelegt. Jetzt
warten wir auf ein verbindliches OK von Finanzamt und Gericht, dann wird
flipdot e.V. gegründet. Dies wird nicht mehr dieses Jahr klappen, dafür
aber direkt im Januar.

Das heutige Treffen findet wegen einer raumgreifenden Ausstellung
**nicht im Kunsttempel** statt, sondern wie immer um 19:00 wieder im
Ausweichquartier in der Pizzeria Da Toni, www.pizza-datoni.de,
Friedrich-Ebert-Straße 26, 34117 Mitte, Kassel 0561 12001.
