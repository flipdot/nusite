+++
title = "Making rulez!"
date = 2009-09-30T16:52:57Z
author = "typ_o"
path = "/blog/2009/09/30/making-rulez"
aliases = ["/blog/archives/14-Making-rulez!.html"]
+++
Auf der SigInt hielt Philip Steffan von
[Bausteln.de](https://bausteln.de/) einen flotten Vortrag unter dem
Titel: "[Es gibt keinen Löffel? Dann druck doch einen Neuen
aus!](https://sigint.ccc.de/sigint/2009/Fahrplan/events/3197.en.html)".
Er beschreibt sehr schön, wie das Selbermachen jetzt gerade von der Welt
der Bits in die Welt der Atome übergeht. Natürlich wurde auch bisher
schon mit Atomen gebaustelt, aber jetzt gibt es "Adapter" zwischen den
Bits und den Atomen: Fräsen, 3D-Drucker etc. Video im direkten Download
[hier](ftp://ftp.ccc.de/events/sigint09/video/SIGINT09_3197_de_es_gibt_keinen_loeffel_dann_druck_doch_einen_neuen_aus.mp4).
