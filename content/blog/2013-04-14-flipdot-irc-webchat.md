+++
title = "flipdot irc webchat"
date = 2013-04-14T02:42:49Z
author = "typ_o"
path = "/blog/2013/04/14/flipdot-irc-webchat"
aliases = ["/blog/archives/194-flipdot-irc-webchat.html"]
+++
Wir haben gestern drüber gesprochen, dass wir IRC mal verstärkt benutzen
wollen in den Zeiten, in denen der Space geschlossen ist ... Als
leichten Zugang bevorzugen wir den Webchat, aber natürlich auch mit dem
Client deiner Wahl [direkt](irc://irc.freenode.net/flipdot).
