+++
title = "Neuer Raspberry - auch diesmal eine gute Entscheidung"
date = 2014-11-10T09:27:49Z
author = "typ_o"
path = "/blog/2014/11/10/neuer-raspberry-auch-diesmal-eine-gute-entscheidung"
aliases = ["/blog/archives/260-Neuer-Raspberry-auch-diesmal-eine-gute-Entscheidung.html"]
+++
Es gibt einen weiteren [Pi mit abgespecktem Hardwareumfang, den
A+](https://www.raspberrypi.org/raspberry-pi-model-a-plus-on-sale/). Was
ich bei den Entscheidungen für den Pi B, A, B+ und jetzt A+ besonders
schätze: Keine geänderten Chips, alle Software läuft wie gehabt. Die
Macher vermeiden, die Hardware Base in dutzende Versionen zu
zersplittern und machen so den Umgang mit den Boards besonders für
Einsteiger einfach, der Support durch die riesige User Base bleibt
einheitlich und passend. (Power User, die Grafik- oder CPU-leistung,
LAN-Bandbreite oder viel RAM brauchen, kaufen halt was anderes)
