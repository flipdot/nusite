+++
title = "Erstes Werkstück gefräst"
date = 2015-06-03T04:57:33Z
author = "typ_o"
path = "/blog/2015/06/03/erstes-werkstuck-gefrast"
aliases = ["/blog/archives/278-Erstes-Werkstueck-gefraest.html"]
+++
[![](/media/pizzaschieber.serendipityThumb.jpg)](/media/pizzaschieber.jpg)  
Ein Pizzaschieber. Sperrholz - das erste Werkstück muss ja nicht gleich
aus Metall sein. Das ertse Lehrgeld bereits bezahlt: Zu wenig Vorschub
und zu hohe Drehzahl - Fräser angelaufen (zu heiss und deswegen
verfärbt, was die Härte des Fräsers vermindert)

Ach zum Backen: Ich will hier noch für das hervorragende
[Plötzblog](https://www.ploetzblog.de/) werben, nach einem Rezept aus
dem Blog habe ich zuletzt ganz hervorragendes Baguette gemacht:

[![](/media/baguette.serendipityThumb.JPG)](/media/baguette.JPG)
