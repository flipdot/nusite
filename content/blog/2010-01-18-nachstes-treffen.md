+++
title = "Nächstes Treffen"
date = 2010-01-18T20:54:48Z
author = "typ_o"
path = "/blog/2010/01/18/nachstes-treffen"
aliases = ["/blog/archives/63-Naechstes-Treffen.html"]
+++
Am Dienstag, 19.1.10 trefen wir uns um 19:00
[hier](https://maps.google.de/maps?q=51.307882,9.488025&num=1&t=h&sll=51.308371,9.489998&sspn=0.006295,0.006295&gl=de&hl=de&ie=UTF8&ll=51.307792,9.488182&spn=0.003367,0.005493&z=18)
wo der grüne Pfeil ist, (also **nicht** im Kunsttempel), direkt vor dem
weiss angemalten Schaufenster des ehemaligen Ladens, und prüfen die
Räume, ob sie für uns taugen. Man findet den Eingang, wenn man von der
Straßenbahnhaltstelle "Weinberg" aus in Richtung Westen die von oben
kommende Frankfurter Straße leicht lebensmüde überquert, und dann die
Rampe zum Philosphenweg hinuntergeht. Nach 10 m ist links die Tür.

Danach haben wir im [San
Marino](https://www.kassel-essentrinken.de/kunden/gastro/KS/st/sanmarino/vk.html)
(Beste Pizzeria Kassles) 12 Plätze reserviert. Interessierte
willkommen!
