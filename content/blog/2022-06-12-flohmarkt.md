+++
title = "Technik-Flohmarkt am 02.07."
date = 2022-06-12T23:20:00Z
author = "jplatte"
path = "/blog/2022/06/12/technik-flohmarkt"
aliases = ["/fm"]
+++

![Flyer Technik-Flohmarkt 02.07.](/media/2022-06-12-flohmarkt.jpg)

Der flipdot hackerspace kassel veranstaltet einen kostenfreien Flohmarkt.

Dein Keller platzt von Elektronik-, Elektrik- und Mechanikmaterial? Oder du
brauchst Material für deine Bastelprojekte im Winter? Du suchst diesen EINEN
komischen Stecker? Hier bist du richtig!

Technik Flohmarkt Kassel, 2. Juli 2022 ab
<time datetime="2022-07-02T10:00:00">10:00 Uhr</time>  
[Franz-Ulrich-Straße 18, 34117 Kassel](/kontakt/#Anfahrt).
Keine Standgebühr, Eintritt kostenlos.
