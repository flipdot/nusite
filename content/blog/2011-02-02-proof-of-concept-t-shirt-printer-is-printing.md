+++
title = "Proof of concept: T-Shirt Printer is printing"
date = 2011-02-02T05:16:12Z
author = "typ_o"
path = "/blog/2011/02/02/proof-of-concept-t-shirt-printer-is-printing"
aliases = ["/blog/archives/126-Proof-of-concept-T-Shirt-Printer-is-printing.html"]
+++
Last night in flipdot - hackerspace kassel: The T-Shirt Printer dit its
first artwork: Printing with an marker pen on a real shirt. The printer
firmware has a lot of headroom for improvement - actually its pretty
dumb. But the java application converting an jpg to an stream of ascii
characters according to the brightness of each pixel works well (thanks
[elektrowolle](https://blog.elektrowolle.de/)!)
