+++
title = "Feuerwerk geht auch anders…"
date = 2024-01-07T15:44:18.493Z
author = "and…"
path = "/blog/2024/01/02/1d-fireworx"
draft = false
+++
unser Urgestein-Member, @hackbroetchen und unendlich viele weitere Namen innehabender genialer Tüftler und Hackster, hat es zu kleiner Berühmtheit gebracht.

Irgendwann war er genervt von dem Geballer an Silvester, wollte da nicht mehr mitmachen und das ganze aber auch nicht völlig unbeteiligt vorbeiziehen lassen. Also was war für ihn näher liegend, als das ganze elektrisch zu veranstalten. Auf Mastodon ging sein Beitrag ziemlich steil, so dass das sogar auf Hackaday gelandet ist, denen ein Beitrag wert war über ihn bzw. sein cooles Projekt berichtet haben.

Hier der Beitrag: [https://hackaday.com/2024/01/06/1d-fireworks-are-nice-and-quiet/](https://hackaday.com/2024/01/06/1d-fireworks-are-nice-and-quiet/)

[und hier mehr von ihm](https://daniel-westhof.de/)

[oder direkt Mastodon](https://chaos.social/@daniel_westhof)

![](/media/1d-fireworks-inner.gif)
