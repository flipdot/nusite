+++
title = "Aufzeichnungen Hackumenta 2019"
date = 2020-02-28T12:38:27Z
author = "Baustel"
path = "/blog/2020/02/28/aufzeichnungen-hackumenta-2019"
aliases = ["/blog/archives/436-Aufzeichnungen-Hackumenta-2019.html"]
+++
[![hackumenta - 10 years in space](/media/c1c629bd33a4aafe8494575e9224b733468eb08f.svg)](https://media.ccc.de/b/conferences/hackumenta)

Im Oktober letzten Jahres hatten wir unsere Jubiläumsfeier (siehe
[Blogeintrag](https://flipdot.org/blog/archives/430-Hackumenta-vom-03.-06.-Oktober.html "Hackumenta vom 03. - 06. Oktober")).
Einige der gehaltenen Talks haben wir aufgezeichnet. Das C3VOC hat uns
ermöglicht, unsere Videos auf
[media.ccc.de](https://media.ccc.de/b/conferences/hackumenta "Hackumenta auf media.ccc.de")
zu veröffentlichen. Schaut doch mal rein!

Hier eine kleiner Auszug:

### Chemie und Physik beim Kochen und Brot backen

<iframe src="https://media.ccc.de/v/hackumenta-10-chemie-und-physik-beim-kochen-und-brot-backen/oembed" allow="gyroscope; picture-in-picture" allowfullscreen=""></iframe>

### CUDA Basics

<iframe src="https://media.ccc.de/v/hackumenta-8-cuda-basics/oembed" allow="gyroscope; picture-in-picture" allowfullscreen=""></iframe>

### Esperanto: Wie funktioniert eine Plansprache?

<iframe src="https://media.ccc.de/v/hackumenta-12-esperanto-wie-funktioniert-eine-plansprache-/oembed" allow="gyroscope; picture-in-picture" allowfullscreen=""></iframe>

Mehr ansehen: <https://media.ccc.de/b/conferences/hackumenta>
