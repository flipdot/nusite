+++
title = "flipdot Hackspace Kassel"
extra.tagline = "CCC-Erfa, Hackspace"
extra.location = "Kassel, [nahe Lutherplatz](/kontakt/#Anfahrt)"
+++

Im flipdot treffen sich Menschen, die an Wissenschaft, Technik, Kunst, (Netz-)Politik, Do-It-Yourself, und vielen weiteren Themen interessiert sind. </br> 
Stell dir einen Hackspace als eine Art offene Werkstatt vor, in der du vieles vorfindest, das Dir bei der Arbeit an einem Projekt hilft: Werkzeuge, Maschinen, Wissen und vor allem nette Leute, die Dir dieses auch weitergeben. flipdot ist eine Art anarchistische Volkshochschule. </br> 
 </br> 
Mehr dazu in der [FAQ](https://flipdot.org/faq/).
 </br> 
 ___