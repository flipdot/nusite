+++
title = "Kontakt"

# Custom template for custom CSS
template = "pages/kontakt.html"
+++

Komm am besten an einem Dienstag ab 19 Uhr vorbei. Das ist unser *offener Tag*,
an dem auch immer viele Member im Space sind.

Wir freuen uns über jeden neuen Besuch, also keine falsche Scheu. Wenn du den
Raum betrittst, passiert normalerweise: nichts. Wundere dich also nicht, sondern
[sprich jemanden an](https://web.archive.org/web/20180207085018/https://store.xkcd.com/products/just-shy)!

Wenn du vorher mit uns Kontakt aufnehmen willst, melde dich einfach per Mail
oder in in unserem [Matrix-Chatraum][matrix].

## Details

* Email: `com[ät]flipdot.org` (Wird von ausgewählten Mitgliedern gelesen und wird archiviert)
* Telefon: +49 561 47395848
* Matrix: [#general:flipdot.org][matrix], Details und Hilfe [hier][matrix-details]
* Mumble: `mumble.flipdot.org`, [Details hier][mumble-details]
* Telegram Gruppe: siehe [hier][telegram-details]

[matrix]: https://matrix.to/#/#general:flipdot.org?via=flipdot.org&via=matrix.org
[matrix-details]: /matrix/
[mumble-details]: /mumble/
[telegram-details]: /matrix/#Telegram

## Anfahrt

[![Umgebungskarte mit ÖPNV-Haltestellen](/media/kontakt_map.png)](/media/kontakt_map.png)

```
Schillerstraße 25
34117 Kassel
```

Koordinaten: [51.32077, 9.49530][geo] oder direkt zu [OpenStreetMap][osm].

[osm]: https://www.openstreetmap.org/node/11425634567/
[geo]: geo://51.32077,9.49530

### ÖPNV

Folgende Stationen sind bei uns in der Nähe.

* Erzbergerstraße
  <b class="public-transport bus">10</b>
  <b class="public-transport bus">16</b>
* Lutherplatz:
  <b class="public-transport tram">7</b>
* Am Stern:
  <b class="public-transport tram">1</b>
  <b class="public-transport tram">3</b>
  <b class="public-transport tram">4</b>
  <b class="public-transport tram">5</b>
  <b class="public-transport tram">6</b>
  <b class="public-transport tram">7</b>
  <b class="public-transport tram">8</b>
  <b class="public-transport tram">RT1</b>
  <b class="public-transport tram">RT4</b>
  <b class="public-transport bus">10</b>
  <b class="public-transport bus">16</b>
  <b class="public-transport bus">30</b>
  <b class="public-transport bus">37</b>
  <b class="public-transport bus">52</b>
  <b class="public-transport bus">100</b>
  
## Leihrad

In Kassel gibt es auch den [Nextbike-Fahrradverleih](https://www.nextbike.de/de/kassel/),
welcher auch direkt gegenüber vom Space  eine Abstellstation hat.
