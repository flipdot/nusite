+++
title = "Raspberry Pi 4 Ubuntu Server 64bit"
+++

Es gibt momentan (2020-02-25) ein 64Bit Ubuntu für RAspi 4, Ubuntu
Server: <https://ubuntu.com/download/raspberry-pi>

- Image:
  <https://ubuntu.com/download/raspberry-pi/thank-you?version=19.10.1&architecture=arm64+raspi3>
  - Seite zeigt auch Desktop Install-beschreibung (Lan am Pi benötigt)
  - und Infos zu Deskop Versionen
    <https://www.linuxtrainingacademy.com/install-desktop-on-ubuntu-server/>
  - Bei der Passworteingabe auf Zahnrad und Desktop einstellen!!!
- Info Sammlung:
  <https://jamesachambers.com/raspberry-pi-ubuntu-server-18-04-2-installation-guide/>
