+++
title = "Abluftanlage"
+++

Hier soll unsere Abluftanlage geplant werden.

## Hacker

- Wolfi
- IO
- ...

## Funktion

Die Anlage soll folgende Dinge entsorgen:

- Küchendämpfe
- Lötdämpfe
- Staub in der Werkstatt

## Dimensionierung

Die Anlage soll geräuscharm, effektiv und kostengünstig sein. Am besten
verläuft sie ohne Biegungen gerade von der Küche im Flur durch den
Lötraum in die Werkstatt aus dem Fenster. Dazu gibt es nur eine kleine
Abzweigung in die Sofaecke im Hauptraum.

<!-- {{attachment:abluft.png}} -->

## Schwierigkeiten

- Schalldämpfung
- Luftstromführung soll aeordynamsich sein (keine harten T Abzweigungen)
- Mehrere Wände müssen durchbrochen werden (ist mit Vermieter abgesprochen)
- Ausleitung über ein Fenster in der Werkstatt (statt Fenster isoliertes Brett mit Loch einsetzen)
- **Stahlträger** liegen im Weg (muss unterhalb laufen)
- Ventilator muss effektiv stark sein
- Bleihaltige Lötdämpfe brauchen einen Filter und müssen von Tischhöhe hochgeführt werden
- Staubhaltige Werkstattluft braucht einen Filter, der Partikel absondert

## Infos

- <http://www.trox-tlt.de/de2/service/download_center/structure/Fachartikel/07fachartikel_grundlagen_der_ventilatorentechnik.pdf>
  (siehe Seite 11)
- <http://www.druckverlust.de/Online-Rechner/Luft.html>

## Material

- ca. 8x [Flachkanal 100 x 22 x 5,4 cm, 600 m³ / h](http://www.obi.de/decom/product/OBI_Vierkantrohr_1_m_System_125/5160239)
  13,99 € (ca. 120 €)
- Kurven für System 125
- Außenklappe
- Filter mit Gitter
- Brett mit Ausgangsloch (passend für Fensterrahmen in der Werkstatt)
- Schneckenhausventilator
- Dunstabzugshaube
