+++
title = "CNC-Fräse: Schnittwerte"
+++

## Fräsertypen

Nach Material

- [Schnellarbeitsstahl](https://de.wikipedia.org/wiki/Schnellarbeitsstahl) (HSS oder nach neuer
  Normung HS) HSS sind sehr wärmeempfindlich, für Metall (Wärmeabfuhr über Späne & Kühlmittel)
- [Hartmetall](https://de.wikipedia.org/wiki/Hartmetall) (VHM sind Vollhartmetallwerkzeuge. Also
  alles besteht aus diesen Werkstoff=Schaft + Schneiden. HM sind Werkzeuge wo der Schaft und die
  Schneiden aus unterschiedlichen Werkstoffen bestehen) (HW = engl. "hardmetal for wood")

Nach Zweck

- [HPC](https://de.wikipedia.org/wiki/High_Performance_Cutting)
  (High Performance Cutter) = Schruppen
- Schlichten

### Alu

Unbeschichteter VHM Fräser Zweischneider

Kühlen mit einer Mischung aus 1/3 Spiritus, 2/3 Wasser, Sprühflasche
vorhanden (Keine Überschwemmung machen, Opferplatte quillt sonst auf)

### Holz

Holz fräsen -\> VHM Fräser

### Kunststoff

Bisher nur wenig Erfahrung, Drehzahl eher niedrig wählen, da sonst der
Kunststoff aufschmelzen kann. Einschneider => gute Spanabfufuhr.

### Fräser kaufen

Beste Erfahrung bisher mit "[2-Schneider
UNI](http://www.sorotec.de/shop/Zerspanungswerkzeuge/SCHAFTFRAeSER/Schaftfraeser-UNI-299/)",
VHM-Schaftfräser von Sorotec. Verwendet in 4 mm und 6 mm Durchmesser, in
Holz, Alu. Für 3D Konturen müssten diese
[VHM-Radienfräser](http://www.sorotec.de/shop/Zerspanungswerkzeuge/RADIENFRAeSER/Radienfraeser-UNI/)
taugen (ungetestet)

## Schnittgeschwindigkeiten (Drehzahl, Zustellung, Vorschub)

Drehzahlrechner <http://www.cenon.de/cgi-bin/ToolCalc?lng=de>
<http://www.precifast.de/schnittgeschwindigkeit-beim-fraesen-berechnen/>

<https://www.stepcraft-systems.com/images/SC-Service/SC_Fraesparameter.pdf>

<http://www.sorotec.de/webshop/Datenblaetter/fraeser/schnittwerte.pdf>

Erfahrungswerte

### Holz (MDF, Multiplex, Sperrholz, Siebdruckplatte)

- 6 mm HM-Fräser,
  - Drehzahl ca "7"
  - Vorschub 2000 mm / min
  - Eintauchgeschwindigkeit 200 mm /min
  - Zustellung max 6 mm (Materialabnahme in einem Durchgang)

### Alu

- 3 mm HM-Fräser (Farbring)
  - Drehzahl ca "4"
  - Vorschub 400 mm / min
  - Eintauchgeschwindigkeit 200 mm /min
  - Zustellung max 1,5 mm (Materialabnahme in einem Durchgang)
  - Kühlen mit mäßig Spiritus (Sprühflasche) Obacht: Sprühnebel -\> Fräsmotor -\> alles Bäm!

### Kunststoff

- 3 mm HM-Fräser (Farbring)
- Drehzahl ca "3-4"
- Vorschub 300 mm / min
- Eintauchgeschwindigkeit 200 mm /min
- Zustellung max 1,5 mm (Materialabnahme in einem Durchgang)
- Kühlen mit mäßig Wasser (Spiritus versprödet z.B. Plexiglas) oder ungekühlt
