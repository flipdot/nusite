+++
title = "Ansprechpartner"
+++

## Neue Member

Du willst Member werden oder hast bereits den Wisch dafür ausgefüllt?

- soerface
- typ_o (Memberliste, Begrüßungsmail)

## Rechnungen

Du hast etwas für flipdot gekauft und das Geld ausgelegt.

- cfstras

## Smartmeter / Stromzähler

Zählt den gesamten Space.

- Initiator: typ_o
- Umsetzung: Wolfi
- Code: Diverse
  - <https://github.com/flipdot/smartmeter>
- Läuft auf power-pi.fd
- Ansprechpartner:
  - typ_o
- Daten:
  [stats.flipdot.org](https://stats.flipdot.org/d/000000004/power-consumption?orgId=1)
- Daten (raw): <http://infragelb.de/flipdot-power/>

## Türschloss

[Projektseite](/projekte/tür/)

- Initiator: typ_o
- Ansprechpartner: Schloss unten (GSM) typ_o, Schloss oben (SSH): malled
- Eigener Zugang (nach 3 Monaten Mitgliedschaft):
  - per Handynummer (unten): typ_o
  - per SSH (oben): Ldap Freischaltungen: cfstras
  - per flipdot-App: n.n.

## CAN-Bus

[Projektseite](/projekte/canberry/) **Deprecated, wird durch
ESP Nodes / MQTT abgelöst.**

- Initiator: typ_o (Hardware), Daniel Huhn, malled (Software)
- Code: <https://github.com/flipdot/Spacecontrol/tree/master/CanBusServer>

## Heizungssteuerung / Thermostat

- Initiator: malled
- Ansprechpartner: Daniel Huhn, malled
- Code: <https://github.com/flipdot/OpenHR20-Firmware>

## flipdot.org Webseite

- Initiator: feliks (Blog, Wiki)
- Ansprechpartner: feliks (Server Zugang), typ_o (Blog-Content, wenn
  du was posten willst)
- Benutzer-Online-Liste: malled

## forum.flipdot.org Forum

- Serveradmin: cfstras
- Forenadmin: feliks, cfstras

## Android App

- Initiator: Daniel Huhn, Ansprechpartner: soerface
- Code: <https://github.com/flipdot/android-app>
- App Store:
  <https://play.google.com/store/apps/details?id=org.flipdot.flipdotapp>

## Router / Netzwerk / VPN

- Initiator: malled
- Ansprechpartner:
  - Lokales Netz: malled
  - Nachrichtenmeisterei-Netzwerk: Wolfi

## Küche

[Projektseite](/projekte/küche/)

- Initiator: kssoz
- Ansprechpartner: and, typ_o

## IoT / IoD / MQTT

[Projektseite](/projekte/iot/)

- Ansprechpartner: feliks, typ_o, jplatte

## flipbot / Sopel

- Initiator: malled

## Kodi (ELEC) am Beamer

- <http://openelec.fd:8080>
- Ansprechpartner: malled
