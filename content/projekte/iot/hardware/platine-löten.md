+++
title = "IoT-Hardware: Platine Löten"
+++

Schritte der Bestückung

## Leere Platine:

<!-- {{attachment:IoD-00.jpg}} -->

## Brücken aus 0,6 mm versilbertem Schaltdraht:

<!-- {{attachment:IoD-01.jpg}} -->

## Brücken gelötet:

<!-- {{attachment:IoD-02.jpg}} -->

## Widerstände:

<!-- {{attachment:IoD-03.jpg}} -->

## Kondensatoren 100 nF

Wenn die Controller noch nicht programmiert sind ( = auf der Platine
programmiert werden sollen), dann den Kondensator C3 links an der
sechspoligen Steckerleiste erstmal nicht bestücken - der liegt an der
ISP - Leitung MOSI, und stört manche Programmer.

<!-- {{attachment:IoD-05.jpg}} -->

## Schottky - Diode. Kathodenstrich rechts

<!-- {{attachment:IoD-06.jpg}} -->

## Kondensator 100 µF und Lötbrücke für Jumper 1

<!-- {{attachment:IoD-07.jpg}} -->

## Mikrocontroller, Pin 1 unten rechts

<!-- {{attachment:IoD-08.jpg}} -->

## Jumper 3 (WiFi Setup)

<!-- {{attachment:IoD-09.jpg}} -->

## Buchsenleiste, 2x4 Pole abschneiden und anwinkeln

<!-- {{attachment:IoD-10.jpg}} -->

## Buchsenleiste eingelötet

<!-- {{attachment:IoD-11.jpg}} -->

## ISP - Stecker

<!-- {{attachment:IoD-12.jpg}} -->

## Stecker für Sensor und Batterie

Je nach Gehäuse kann es sinnvoll sein, die drei Pins für den Anschluß
des Sensors (Umschalter) abzuwinkeln.

<!-- {{attachment:IoD-13.jpg}} -->

