+++
title = "IoT: Beschaffung"
+++

- Geeignete Mikro- / Reed-Schalter für die entspr. Aufgaben
  - Fenster / Dachluke / Kühlschrank
  - <https://www.reichelt.de/Schnappschalter-Endschalter/MAR-1006-1001/3/index.html?ACTION=3&GROUPID=7599&ARTICLE=166850&OFFSET=75&>
  - Schnappschalter 1xUM 10A-400VAC Rollenhebel 2,99 €, Vorlaufweg
    max. 3,9 mm, Nachlaufweg min. 1,6 mm (Große Wege - einfache
    Justage)
  - Beim Chinamann für
    [1/10](https://www.aliexpress.com/item/10-Pcs-Roller-Lever-Arm-SPDT-NO-NC-Momentary-Micro-Switches-V-156-1C25/32643444249.html)
    Preis oder für
    [1/20](https://www.aliexpress.com/item/Wholesale-10pcs-lot-New-Micro-Roller-Lever-Arm-Normally-Open-Close-Limit-Switch-KW12-3/32613291715.html)
  - Reed Switches NO, für Öffnerkette am Aussenfenstern (Erfordert Pullup weil kein Wechsler)
    [1](https://www.aliexpress.com/item/5pcs-Adhesived-Security-Door-Window-Magnetic-Contact-Reed-Switch-NO/32795672550.html),
    [2](https://www.aliexpress.com/item/Free-Shipping-10pc-Normally-open-reed-switch-proximity-switches-plastic-package-with-mounting-hole-and-wire/1342008298.html).
  - Reed Wechsler, selber in Gehäuse bauen?
  - Überlegung: Betätigungsstück für Mikroschalter am Fenster etc auf
  - <https://www.reichelt.de/Kabelbinder-Montagesockel/KAB-SOCKEL-4-8HS/3/index.html?ACTION=3&GROUPID=5737&ARTICLE=45711&OFFSET=75&>
  - Kabelsockel für Binder 4,8mm, sw 100er-Pack befestigt? Andere Lösung?
- Geschirmte Leitung Schalter -\> IoD Knoten
  - <https://www.reichelt.de/Mikrofon-Videoleitung/ML-209-25/3/index.html?ACTION=3&GROUPID=5032&ARTICLE=13309&OFFSET=75&>
    Mikrofonleitung, 2x0,08mm², rund, 25m-Ring 14,95 €. (µC Eingang
    Hochohmig wegen Verzicht auf Pullup/down)
