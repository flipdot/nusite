+++
title = "Merge"
+++

## Abgeschlossen

flipdot ist ERFA Kassel, entsendet Vertreter zu den ERFA Treffen, etc.

Die Unigruppe in der Ingschule bleibt als Chaostreff bestehen

------------------------------------------------------------------------

**Ziel:**Zusammenschluss von CCCKS und fd // **Zweck:**Beseitigung von
Problem im CCCKS (ist kein Verein), gemeinsame Mittel für CCCKS-fd aus
CCC Topf

**Was ist nötig für einen Zusammenschluss?**

- Beschluss der CCCKS Member
- Beschlus der fd Member
  - Satzungsänderung fd Satzung

**Was verändert sich für CCCKS?**

- CCCKS wird gemeinnütziger Verein
- ERFA-Rückerstattung steht auf sauberer Grundlage
- CCCKS Member werden Member des neuen Vereins
- Aussicht auf neue, gemeinsame Räume, die 24/7 zugänglich sind und
  mehr Möglichkeiten bieten

**Was verändert sich für CCCKS nicht?**

- Räume in der Uni bleiben (wäre dumm, die aufzulösen)

**Was verändert sich für fd?**

- fd wird CCC ERFA Kreis
- Möglichkeit, mit gemeinsamer Kraft und Mitteln endlich größere Räume
  zu suchen

**Was verändert sich für fd nicht?**

- Ziele des Vereins
- Vermutlich nicht mal die Satzung, da Quelle CCC Bremen

**Was muss noch geklärt werden?**

- Ist eine Satzungsänderung überhaupt nötig?
- Welche Mitgliedsbeiträge sollen in Zukunft gelten, ist für
  CCCKS-Member, im besonderen Studenten, eine Regelung nötig, oder
  reicht die fd-Ermäßigung?
- Wer stimmt ab und entscheidet über den Zusammenschluss
  - fd Member
  - CCCKS Member
- Ist eine Namensänderung nötig?
  - Aufwand?
  - Folgen davon?
- Ist der Uni-Raum dann noch offizieller CCC Treff, oder sollte der
  eher so unter dem Radar fliegen?
