+++
title = "Linux Boot Stick: EFI-Tricks - Wenn Bootstick nicht startet"
+++

Manche alte Laptops wie Convertibles sind im Bios kastriert, so dass man
einen normalen Linux Boot Stick nicht ohne weiteres starten kann.

Übersicht

1\. Bootstickgröße

2\. Bios Einstellungen zu Secure Boot...

3\. Partition Table speziell

1\. Bootstickgröße oft max 4gb oder 8gb oder Partitionsgröße begrenzen

2\. Secure Boot: ENABLED

Secure Boot Status: Disabled Platform Mode: Setup Mode Secure Boot Mode:
Custom Mode

IPM Support: Enabled

3\.

Der Stick muss ein spezielles Format haben und eine spezielle neue efi
Datei soll dabei sein:

Weiter unten zitiert die Komplette Anleitung von
<https://forums.linuxmint.com/viewtopic.php?f=42&t=216857>

Bei einem alten medion e1240t mit abnehmbarem Bildschirm, scheint die
Installation nur zu klappen, wenn man beim Ziellaufwerk vorher die
Partition Table als GPT schreibt. Löscht vorher alles wenn man mit
gparted die Table neu schreibt. Auch braucht man zwei PArtitionen: Eine
kleine bsp 750MB als efi laufwerk, das auch mit flag als"esp" und
dadurch gleichzeitig als ""boot" setzt. Die zweite z.B. als ext4 für
das Linux, was als / eigehangen werden muß (option wählen).

Dadurch scheint er beim neu schreiben der Partitionen etwas anders zu
machen als bei der normalen Installation.

The Linux Mint team say that the Linux Mint 17.x 32bit version can\'t be
use in UEFI mode. Only in BIOS mode. Now it is possible because I found
a solution with the help of internet.

Here is the solution

You need four things:

\- An Usb key. - The bootia32.efi file. - The Linux Mint 17.x 32bit
version. - Gdisk must be installed.

1\. Download the Linux Mint 17.x 32bit version in one of your home
directory (for example: Downloads).

2\. Find the bootia32.efi file on internet. You can find it here :
<http://webativo.com/uploads/files/usb-pack_efi.zip> (unzip this file
and search bootia32.efi in /usb-pack_efi/EFI/BOOT/ directory). Or here :
<https://github.com/jfwells/linux-asus-t> ... otia32.efi (click on the
raw button). If you want to create your own bootia32.efi file, check in
the first source link below.

3\. Plug your usb key. Check the letter of your device (via menu -\>
preferences -\> disks).

4\. Open a terminal window and make the following lines :

Code:

```
sudo sgdisk --zap-all /dev/sdx
sudo sgdisk --new=1:0:0 --typecode=1:ef00 /dev/sdx
sudo mkfs.vfat -F32 /dev/sdx1
```

Note: x must be the letter of your usb key. These commands create the
GPT partition table on the usb key, an efi partition for the whole usb
key (ef00) and a fat32 partition for sdx1.

5\. Mount the fat32 partition on /mnt mount point via that command line:

Code:

```
sudo mount -t vfat /dev/sdx1 /mnt
```

Note: x must be the letter of your usb key.

6\. Unzip the 32bit version iso on /mnt :

Code:

```
sudo 7z x /home/nameofyouruser/Downloads/linuxmint-17.x-xxxx-32bit.iso
-o/mnt/
```

7\. Create the /EFI/BOOT/ directory in /mnt mount point :

Code:

```
sudo mkdir /mnt/EFI sudo mkdir /mnt/EFI/BOOT/
```

Now you\'ve got an /EFI/BOOT/ directory that didn\'t exist before.

8\. Copy the bootia32.efi file in `/EFI/BOOT/`:

Code:

```
sudo cp bootia32.efi /mnt/EFI/BOOT/
```

9\. Umount the /mnt mount point :

Code:

```
sudo umount /mnt
```

10\. Reboot the computer.

Enjoy !!!!

The links that help me: <https://github.com/lopaka/instructions/> ...
-x205ta.md <http://ubuntuforums.org/showthread.php?t=2276498>
