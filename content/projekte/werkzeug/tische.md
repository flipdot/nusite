+++
title = "Werkzeug: Tische"
+++

Ideal: Tische mit Klapp - Untergestell. Kosten allerdings \> 100 Geld.
Bei nicht allzu häufigem Umbau sind auch abschraubbare Tischbeine ok.

[Beine](https://www.amazon.de/Tischbeine-4er-Set-H%C3%B6he-Edelstahl/dp/B007FE45EG/ref=pd_cp_60_4?ie=UTF8&psc=1&refRID=QDNBJG6MTC8KJ1AGA2ZP),
haben sehr gute Bewertungen bei big A.

Platten aus Buche Multiplex, an den Rändern aufgedoppelt, oder was
Billigeres aus dem Baumarkt.
