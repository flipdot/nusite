+++
title = "IoT"
+++

Unter-Seiten:

* [Anforderungen](anforderungen/)
* [Beschaffung](beschaffung/)
* [Etat](etat/)
* [Hardware](hardware/)
* [MQTT-Topics](mqtt-topics/)
* [Netzwerk](netzwerk/)
* [Nodes](nodes/)
* [Software](software/)
* [ToDo](todo/)

<!-- {{https://flipdot.org/blog/uploads/IoD.jpg|Internet of Dings!}} -->

Im Space sollen Sensoren und Aktoren ausgebracht werden. Überwachen von
Türen und Fenstern, Kühlschrank, Herd usw., Steuern von Heizung
(und...).

Lightning Talk Hackover 2017: <!-- <attachment:Hackover-IoD.zip> -->
